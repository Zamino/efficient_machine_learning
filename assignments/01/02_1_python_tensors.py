import torch

print("torch.zeros(3):", torch.zeros(3))
print("torch.ones(4):", torch.ones(4))
print("torch.rand(5):", torch.rand(5))
print("torch.ones_like(torch.zeros(3)):", torch.ones_like(torch.zeros(3)))

T = torch.tensor([
        [[1,  9, 17],[5, 13, 21]],
        [[2, 10, 18],[6, 14, 22]],
        [[3, 11, 19],[7, 15, 23]],
        [[4, 12, 20],[8, 16, 24]]
        ])
print("T:", T)
print("T.size():", T.size())

import numpy
A = numpy.array([
        [[1,  9, 17],[5, 13, 21]],
        [[2, 10, 18],[6, 14, 22]],
        [[3, 11, 19],[7, 15, 23]],
        [[4, 12, 20],[8, 16, 24]]
        ])
T_2 = torch.tensor(A)
print("A:", A)
print("T_2:", T_2)

P = torch.tensor([[1, 2], [3, 4]])
Q = torch.tensor([[5, 6], [7, 8]])

print("torch.add:", torch.add(P, Q))
print("torch.mul:", torch.mul(P, Q))
print("+:", P + Q)
print("*:", P * Q)

Q_t = torch.t(Q)
print("Q_t:", Q_t)
print("torch.matmul:", torch.matmul(P, Q_t))
print("@:", P @ Q_t)

print("torch.sum(P):", torch.sum(P))
print("torch.max(P):", torch.max(P))

l_tensor_0 = torch.tensor([1, 2, 3])
print("before l_tensor_0:", l_tensor_0)
l_tmp = l_tensor_0
l_tmp[:] = 0
print("l_tmp:", l_tmp)
print("after l_tensor_0:", l_tensor_0)

print()

l_tensor_1 = torch.tensor([1, 2, 3])
print("before l_tensor_1:", l_tensor_1)
l_tmp = l_tensor_1.clone().detach()
l_tmp[:] = 0
print("l_tmp:", l_tmp)
print("after l_tensor_1:", l_tensor_1)


print("T:", T)
print("T.size():", T.size())
print("T.stride():", T.stride())
print("T.dtype:", T.dtype)
print("T.layout:", T.layout)
print("T.device:", T.device)

l_tensor_float = torch.tensor(T, dtype=torch.float)
print("l_tensor_float.dtype:", l_tensor_float.dtype)

l_tensor_fixed = l_tensor_float[:,0,:]
print("l_tensor_fixed:",          l_tensor_fixed)
print("l_tensor_fixed.size():",   l_tensor_fixed.size())
print("l_tensor_fixed.stride():", l_tensor_fixed.stride())
print("l_tensor_fixed.dtype:",    l_tensor_fixed.dtype)
print("l_tensor_fixed.layout:",   l_tensor_fixed.layout)
print("l_tensor_fixed.device:",   l_tensor_fixed.device)

l_tensor_complex_view = l_tensor_float[::2,1,:]
print("l_tensor_complex_view:",          l_tensor_complex_view)
print("l_tensor_complex_view.size():",   l_tensor_complex_view.size())
print("l_tensor_complex_view.stride():", l_tensor_complex_view.stride())

l_tensor_complex_view_dense = l_tensor_complex_view.contiguous()
print("l_tensor_complex_view_dense:",          l_tensor_complex_view_dense)
print("l_tensor_complex_view_dense.size():",   l_tensor_complex_view_dense.size())
print("l_tensor_complex_view_dense.stride():", l_tensor_complex_view_dense.stride())

import ctypes
l_data_ptr = l_tensor_float.data_ptr()
print("l_tensor_float[0,0,0]:", l_tensor_float[0,0,0]); 
print("l_data_ptr[0,0,0]:", (ctypes.c_float).from_address( l_data_ptr )); 
print("l_tensor_float[0,1,2]:", l_tensor_float[0,1,2]); 
i = 0; j = 1; k = 2;
print("l_data_ptr[0,1,2]:", (ctypes.c_float).from_address( l_data_ptr + (i * 6 + j * 3 + k * 1) * 4 )); 
