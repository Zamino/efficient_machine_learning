# Assignment 01

## Setup

Login on the test server and set the conda envirment.

```bash
$ ssh eml_03@lechuck.inf-ra.uni-jena.de
$ conda activate /opt/eml/envs/pytorch_2.2.2
```

## 2.1 Python

### Creation

#### 1
```python
import torch

print("torch.zeros(3):", torch.zeros(3))
print("torch.ones(4):", torch.ones(4))
print("torch.rand(5):", torch.rand(5))
print("torch.ones_like(torch.zeros(3)):", torch.ones_like(torch.zeros(3)))
```

```python
torch.zeros(3): tensor([0., 0., 0.])
torch.ones(4): tensor([1., 1., 1., 1.])
torch.rand(5): tensor([0.9919, 0.2830, 0.6676, 0.0488, 0.0230])
torch.ones_like(torch.zeros(3)): tensor([1., 1., 1.])
```

#### 2
```python
import torch
T = torch.tensor([
        [[1,  9, 17],[5, 13, 21]],
        [[2, 10, 18],[6, 14, 22]],
        [[3, 11, 19],[7, 15, 23]],
        [[4, 12, 20],[8, 16, 24]]
        ])
print("T:", T)
print("T.size():", T.size())

```

```python
T: tensor([[[ 1,  9, 17],
         [ 5, 13, 21]],

        [[ 2, 10, 18],
         [ 6, 14, 22]],

        [[ 3, 11, 19],
         [ 7, 15, 23]],

        [[ 4, 12, 20],
         [ 8, 16, 24]]])
T.size(): torch.Size([4, 2, 3])
```

#### 3

```python
import numpy
A = numpy.array([
        [[1,  9, 17],[5, 13, 21]],
        [[2, 10, 18],[6, 14, 22]],
        [[3, 11, 19],[7, 15, 23]],
        [[4, 12, 20],[8, 16, 24]]
        ])
T_2 = torch.tensor(A)
print("A:", A)
print("T_2:", T_2)
```

```python
A: [[[ 1  9 17]
  [ 5 13 21]]

 [[ 2 10 18]
  [ 6 14 22]]

 [[ 3 11 19]
  [ 7 15 23]]

 [[ 4 12 20]
  [ 8 16 24]]]
T_2: tensor([[[ 1,  9, 17],
         [ 5, 13, 21]],

        [[ 2, 10, 18],
         [ 6, 14, 22]],

        [[ 3, 11, 19],
         [ 7, 15, 23]],

        [[ 4, 12, 20],
         [ 8, 16, 24]]])
```

### Operations

#### 1

```python
P = torch.tensor([[1, 2], [3, 4]])
Q = torch.tensor([[5, 6], [7, 8]])

print("torch.add:", torch.add(P, Q))
print("torch.mul:", torch.mul(P, Q))
print("+:", P + Q)
print("*:", P * Q)
```

```python
torch.add: tensor([[ 6,  8],
        [10, 12]])
torch.mul: tensor([[ 5, 12],
        [21, 32]])
+: tensor([[ 6,  8],
        [10, 12]])
*: tensor([[ 5, 12],
        [21, 32]])
```

#### 2

```python
Q_t = torch.t(Q)
print("Q_t:", Q_t)
print("torch.matmul:", torch.matmul(P, Q_t))
print("@:", P @ Q_t)
```

```python
Q_t: tensor([[5, 7],
        [6, 8]])
torch.matmul: tensor([[17, 23],
        [39, 53]])
@: tensor([[17, 23],
        [39, 53]])
```

#### 3

```python
print("torch.sum(P):", torch.sum(P))
print("torch.max(P):", torch.max(P))
```

```python
torch.sum(P): tensor(10)
torch.max(P): tensor(4)
```

#### 4

The difference between `l_tmp = l_tensor_0` and
`l_tmp = l_tensor_1.clone().detach()` is that the former creates a reference to
the initial tensor, while the latter creates a copy of the original tensor.
Therefore, when `l_tmp[:] = 0` is executed in the former scenario, it modifies
the original tensor, whereas in the latter scenario, the original tensor remains
unchanged.

```python
l_tensor_0 = torch.tensor([1, 2, 3])
print("before l_tensor_0:", l_tensor_0)
l_tmp = l_tensor_0
l_tmp[:] = 0
print("l_tmp:", l_tmp)
print("after l_tensor_0:", l_tensor_0)

print()

l_tensor_1 = torch.tensor([1, 2, 3])
print("before l_tensor_1:", l_tensor_1)
l_tmp = l_tensor_1.clone().detach()
l_tmp[:] = 0
print("l_tmp:", l_tmp)
print("after l_tensor_1:", l_tensor_1)
```

```python
before l_tensor_0: tensor([1, 2, 3])
l_tmp: tensor([0, 0, 0])
after l_tensor_0: tensor([0, 0, 0])

before l_tensor_1: tensor([1, 2, 3])
l_tmp: tensor([0, 0, 0])
after l_tensor_1: tensor([1, 2, 3])
```

### Storage

#### 1

```python
print("T:", T)
print("T.size():", T.size())
print("T.stride():", T.stride())
print("T.dtype:", T.dtype)
print("T.layout:", T.layout)
print("T.device:", T.device)
```

```python
T: tensor([[[ 1,  9, 17],
         [ 5, 13, 21]],

        [[ 2, 10, 18],
         [ 6, 14, 22]],

        [[ 3, 11, 19],
         [ 7, 15, 23]],

        [[ 4, 12, 20],
         [ 8, 16, 24]]])
T.size(): torch.Size([4, 2, 3])
T.stride(): (6, 3, 1)
T.dtype: torch.int64
T.layout: torch.strided
T.device: cpu
```

#### 2

```python
l_tensor_float = torch.tensor(T, dtype=torch.float)
print("l_tensor_float.dtype:", l_tensor_float.dtype)
```

```python
l_tensor_float.dtype: torch.float32
```

#### 3

When creating a view, the size and stride are modified while the dtype, layout, and device remain unchanged.

```python
l_tensor_fixed = l_tensor_float[:,0,:]
print("l_tensor_fixed:",          l_tensor_fixed)
print("l_tensor_fixed.size():",   l_tensor_fixed.size())
print("l_tensor_fixed.stride():", l_tensor_fixed.stride())
print("l_tensor_fixed.dtype:",    l_tensor_fixed.dtype)
print("l_tensor_fixed.layout:",   l_tensor_fixed.layout)
print("l_tensor_fixed.device:",   l_tensor_fixed.device)
```

```python
l_tensor_fixed: tensor([[ 1.,  9., 17.],
        [ 2., 10., 18.],
        [ 3., 11., 19.],
        [ 4., 12., 20.]])
l_tensor_fixed.size(): torch.Size([4, 3])
l_tensor_fixed.stride(): (6, 1)
l_tensor_fixed.dtype: torch.float32
l_tensor_fixed.layout: torch.strided
l_tensor_fixed.device: cpu
```

#### 4

`::2` specifies that every second column is to be kept. In this case, only columns 0 and 2 are kept. `1` indicates that only column 1 is kept. Using `:` specifies that all columns are kept. Therefore the new tensor has 2 columns in the first rank and 3 in the second rank, corresponding to the third rank of the original tensor.

The new stride is `(12, 1)`. The strip of the first rank is doubled from 6 to 12 as only every second column of the first rank is taken. The 1 is kept in the last position, because the last rank didn't changed.

```python
l_tensor_complex_view = l_tensor_float[::2,1,:]
print("l_tensor_complex_view:",          l_tensor_complex_view)
print("l_tensor_complex_view.size():",   l_tensor_complex_view.size())
print("l_tensor_complex_view.stride():", l_tensor_complex_view.stride())
```

```python
l_tensor_complex_view: tensor([[ 5., 13., 21.],
        [ 7., 15., 23.]])
l_tensor_complex_view.size(): torch.Size([2, 3])
l_tensor_complex_view.stride(): (12, 1)
```

#### 5

`contiguous` creates a contiguous tensor of the original one. As the size of the
tensor is `(2, 3)` the new stride is `(3, 1)`. Torch arranges the elements of the
tensor that the higher the rank the smaller the stride. Therefore the stride
of the rank 2 is 1 and as rank 2 has the size 3 the stride of rank 1 is 3.

```python
l_tensor_complex_view_dense = l_tensor_complex_view.contiguous()
print("l_tensor_complex_view_dense:",          l_tensor_complex_view_dense)
print("l_tensor_complex_view_dense.size():",   l_tensor_complex_view_dense.size())
print("l_tensor_complex_view_dense.stride():", l_tensor_complex_view_dense.stride())
```

```python
l_tensor_complex_view_dense: tensor([[ 5., 13., 21.],
        [ 7., 15., 23.]])
l_tensor_complex_view_dense.size(): torch.Size([2, 3])
l_tensor_complex_view_dense.stride(): (3, 1)
```

#### 6


```python
import ctypes
l_data_ptr = l_tensor_float.data_ptr()
print("l_tensor_float[0,0,0]:", l_tensor_float[0,0,0]);
print("l_data_ptr[0,0,0]:", (ctypes.c_float).from_address( l_data_ptr ));
print("l_tensor_float[0,1,2]:", l_tensor_float[0,1,2]);
i = 0; j = 1; k = 2;
print("l_data_ptr[0,1,2]:", (ctypes.c_float).from_address( l_data_ptr + (i * 6 + j * 3 + k
* 1) * 4 ));
```

```python
l_tensor_float[0,0,0]: tensor(1.)
l_data_ptr[0,0,0]: c_float(1.0)
l_tensor_float[0,1,2]: tensor(21.)
l_data_ptr[0,1,2]: c_float(21.0)
```
