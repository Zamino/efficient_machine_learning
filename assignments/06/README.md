# Assignment 06

## 9.4 Triton

### 0

#### Shapes


# Batch: torch.Size([128, 3, 224, 224])
x = x.view( x.size(0), x.size(1), x.size(2)//16, 16, x.size(3)//16, 16 )
#x: torch.Size([128, 3, 14, 16, 14, 16])
x = x.permute( 0, 2, 4, 3, 5, 1 ).contiguous()
#x: torch.Size([128, 14, 14, 16, 16, 3])
x = x.view( x.size(0), x.size(1)*x.size(2), -1 )
#x: torch.Size([128, 196, 16, 16, 3])
#x: torch.Size([128, 196, 768])

#### 1

`triton_gemm.py`

```
(triton) $ python ./triton_gemm.py
✅ Triton and Torch match
```

