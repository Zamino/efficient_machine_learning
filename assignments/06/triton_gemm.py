import torch

import triton
import triton.language as tl

@triton.autotune(
    configs = [
      triton.Config( {
        'SIZE_BLOCK_L': 1,
        'SIZE_BLOCK_M': 16, # 64
        'SIZE_BLOCK_N': 16, # 64
        'SIZE_BLOCK_K': 16, },
        num_stages = 3,
        num_warps = 2 )
      ],
    key = [ 'size_a_l',
            'size_b_l',
            'size_m',
            'size_n',
            'size_k' ]
    )
@triton.jit
def matmul_kernel(
    # Pointers to matrices
    a_ptr, b_ptr, c_ptr, bias_ptr,
    # Matrix dimensions
    size_a_l, size_b_l,
    size_m, size_n, size_k,
    # Strides
    stride_a_l, stride_a_m, stride_a_k,  # A
    stride_b_l, stride_b_k, stride_b_n,  # B
    stride_c_l, stride_c_m, stride_c_n,  # C
    stride_bias_n,                       # BIAS
    # Meta-parameters
    SIZE_BLOCK_L: tl.constexpr,
    SIZE_BLOCK_M: tl.constexpr,
    SIZE_BLOCK_N: tl.constexpr,
    SIZE_BLOCK_K: tl.constexpr,
    ACTIVATION: tl.constexpr
    ):

  pid = tl.program_id(axis=0)

  #c_pid = tl.zeros((SIZE_BLOCK_M, SIZE_BLOCK_N,), dtype=tl.float32) + pid + 1

  num_blocks_m = tl.cdiv(size_m, SIZE_BLOCK_M)
  num_blocks_n = tl.cdiv(size_n, SIZE_BLOCK_N)

  pid_l = pid // (num_blocks_m * num_blocks_n)
  pid_m = pid %  (num_blocks_m * num_blocks_n)
  pid_m //= num_blocks_n
  pid_n = pid % num_blocks_n

  offs_a_m = (pid_m * SIZE_BLOCK_M + tl.arange(0, SIZE_BLOCK_M)) % size_m
  offs_b_n = (pid_n * SIZE_BLOCK_N + tl.arange(0, SIZE_BLOCK_N)) % size_n

  offs_c_m = pid_m * SIZE_BLOCK_M + tl.arange(0, SIZE_BLOCK_M)
  offs_c_n = pid_n * SIZE_BLOCK_N + tl.arange(0, SIZE_BLOCK_N)

  offs_bias_n = pid_n * SIZE_BLOCK_N + tl.arange(0, SIZE_BLOCK_N) % size_n

  offs_l = pid_l * SIZE_BLOCK_L
  offs_k = tl.arange(0, SIZE_BLOCK_K)

  offs_bias_n = (pid_n * SIZE_BLOCK_N + tl.arange(0, SIZE_BLOCK_N)) % size_n

  a_base_ptrs = a_ptr \
              + (offs_a_m[:, None] * stride_a_m) \
              + (offs_k[None, :]   * stride_a_k)

  b_base_ptrs = b_ptr \
              + (offs_k[:, None]   * stride_b_k) \
              + (offs_b_n[None, :] * stride_b_n)

  if (bias_ptr is not None):
    bias_ptrs = bias_ptr \
              + (offs_bias_n * stride_bias_n)
    bias = tl.load(bias_ptrs)

  c_base_ptrs = c_ptr \
         + stride_c_m * offs_c_m[:, None] \
         + stride_c_n * offs_c_n[None, :]
  c_mask = (offs_c_m[:, None] < size_m) & (offs_c_n[None, :] < size_n)

  for id_l in range (0, SIZE_BLOCK_L):

    a_ptrs = a_base_ptrs \
           + (offs_l + id_l) * stride_a_l # stride_a_l is 0 if a has 2 dimensions

    b_ptrs = b_base_ptrs \
           + (offs_l + id_l) * stride_b_l # stride_b_l is 0 if b has 2 dimensions

    accumulator = tl.zeros((SIZE_BLOCK_M, SIZE_BLOCK_N), dtype=tl.float32)

    for id_k in range(0, tl.cdiv(size_k, SIZE_BLOCK_K)):
      k_mask = offs_k[:, None] < size_k - id_k * SIZE_BLOCK_K
      a = tl.load(a_ptrs, mask=k_mask, other=0.0)
      b = tl.load(b_ptrs, mask=k_mask, other=0.0)

      accumulator = tl.dot(a, b, accumulator, allow_tf32 = False)

      a_ptrs += SIZE_BLOCK_K * stride_a_k
      b_ptrs += SIZE_BLOCK_K * stride_b_k

    if bias_ptr is not None:
        accumulator += bias

    if ACTIVATION == "leaky_relu":
      accumulator = leaky_relu(accumulator)
    if ACTIVATION == "gelu":
      accumulator = gelu(accumulator)
    c = accumulator

    # -----------------------------------------------------------
    # Write back the block of the output matrix C with masks.
    c_ptrs = c_base_ptrs + (offs_l + id_l) * stride_c_l
    tl.store(c_ptrs, c, mask=c_mask)


@triton.jit
def leaky_relu(x):
  x = x + 1
  return tl.where(x >= 0, x, 0.01 * x)

# Constant for GELU calculation
k_const = (2.0 / torch.pi) ** 0.5

@triton.jit
def gelu(x):
    t = (x + 0.044715 * x * x * x) * k_const
    numerator = 2
    denominator = (1 + tl.exp(-2 * t))
    gelu_output = 0.5 * x * (1 + (numerator / denominator) - 1)
    return gelu_output


def matmul(a, b, bias=None, activation=""):
  # Check constraints.
  assert ((len(a.shape) ==  2) or (len(a.shape) == 3)), "A has to be a matrix or batched matrix"
  assert ((len(b.shape) ==  2) or (len(b.shape) == 3)), "B has to be a matrix or batched matrix"
  assert ((len(a.shape) ==  2) or (len(b.shape) == 2)), "Only one batch dimension allowed"

  # L_A, L_B, M, N, K
  # stride_a_l, stride_a_m, stride_a_k
  # stride_b_l, stride_b_k, stride_b_n
  if (len(a.shape) == 3):
    assert a.shape[2] == b.shape[0], "Incompatible dimensions"
    L_A, M, K = a.shape
    L_B, (K, N) = 1, b.shape
    stride_a_l, stride_a_m, stride_a_k = a.stride()
    stride_b_l, (stride_b_k, stride_b_n) = 0, b.stride()
  elif (len(b.shape) == 3):
    assert a.shape[1] == b.shape[1], "Incompatible dimensions"
    L_B, (M, K) = 1, a.shape
    L_B, K, N = b.shape
    stride_a_l, (stride_a_m, stride_a_k) = 0, a.stride()
    stride_b_l, stride_b_k, stride_b_n = b.stride()
  else:
    assert a.shape[1] == b.shape[0], "Incompatible dimensions"
    L_A, (M, K) = 1, a.shape
    L_B, (K, N) = 1, b.shape
    stride_a_l, (stride_a_m, stride_a_k) = 0, a.stride()
    stride_b_l, (stride_b_k, stride_b_n) = 0, b.stride()
  L = max(L_A, L_B)
  
  stride_bias_n = 0
  if (bias is not None):
    assert bias.shape[0] == N, "Incompatible dimensions"
    stride_bias_n, = bias.stride()

  assert a.is_contiguous(), "Matrix A must be contiguous"

  # Allocates output.
  c = torch.empty((L, M, N), device=a.device, dtype=torch.float32)
  stride_c_l, stride_c_m, stride_c_n = c.stride()
  # 1D launch kernel where each block gets its own program.
  grid = lambda META: ( \
      triton.cdiv(L, META['SIZE_BLOCK_L']) \
    * triton.cdiv(M, META['SIZE_BLOCK_M']) \
    * triton.cdiv(N, META['SIZE_BLOCK_N']), )
  matmul_kernel[grid](
    a, b, c, bias,
    L_A, L_B,
    M, N, K,
    stride_a_l, stride_a_m, stride_a_k,
    stride_b_l, stride_b_k, stride_b_n,
    stride_c_l, stride_c_m, stride_c_n,
    stride_bias_n,
    ACTIVATION=activation
    )
  return c


batch_size_a = 10
torch.manual_seed(0)
a = torch.randn((batch_size_a, 196, 768), device="cuda", dtype=torch.float32)
b = torch.randn((768, 768), device="cuda", dtype=torch.float32)
bias = torch.randn((768,), device="cuda", dtype=torch.float32)
gelu_func = torch.nn.GELU()
torch_output = torch.zeros((batch_size_a, 196, 768), device="cuda", dtype=torch.float32)

for i in range(0, batch_size_a):
  torch_output[i, :, :] = torch.matmul(a[i, :, :], b)
  torch_output[i, :, :] = torch.add(torch_output[i, :, :], bias)
  torch_output[i, :, :] = gelu_func(torch_output[i, :, :])

triton_output = matmul(a, b, bias, activation="gelu")

if torch.allclose(torch_output, triton_output, atol=1e-2, rtol=0):
    print("✅ Triton and Torch match")
else:
    print("❌ Triton and Torch differ")
