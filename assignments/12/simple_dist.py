import torch

torch.distributed.init_process_group(backend="mpi")
rank = torch.distributed.get_rank()
size = torch.distributed.get_world_size()

print(f"({rank}) 1:")
print(f"({rank}) rank: {rank}")
print(f"({rank}) size: {size}")

print(f"({rank}) 2:")
tensor = None
if (rank == 0):
    tensor = torch.ones([3,4])
else:
    tensor = torch.zeros([3,4])
if (rank == 0):
    torch.distributed.send(tensor, 1)
if (rank == 1):
    torch.distributed.recv(tensor)
print(f"({rank})", tensor)


# print("3:")
# if (rank == 0):
#     tensor = torch.ones([3,4])
# else:
#     tensor = torch.zeros([3,4])
# handler = None
# if (rank == 0):
#     handle = torch.distributed.isend(tensor, 1)
# if (rank == 1):
#     handle = torch.distributed.irecv(tensor)
# print(f"{rank}:", tensor)


print(f"({rank}) 4:")
tensor = torch.tensor([
    [1,  2,  3,  4],
    [5,  6,  7,  8],
    [9, 10, 11, 12]
    ])

torch.distributed.all_reduce(tensor, op=torch.distributed.ReduceOp.SUM)
print(f"({rank})", tensor)
