#!/usr/bin/env bash
##
#SBATCH --job-name=eml
#SBATCH --output=dataloader_dist.out
#SBATCH -p short
#SBATCH -N 4
#SBATCH --cpus-per-task=1
#SBATCH --time=00:10:00

# activate virtual environment
module load anaconda3/2024.02-1
conda activate /work/EML/pytorch_env_new
module load mpi/openmpi/4.1.1

mpirun -n 4 --host "localhost:4"  python dataloader_dist.py
