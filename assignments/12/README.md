# Assignment 12

## 13. Distributed Deep Learning

### 13.1. The Scenic Route

#### 1, 2, 4

`simple_dist.py`
```python
import torch

torch.distributed.init_process_group(backend="mpi")
rank = torch.distributed.get_rank()
size = torch.distributed.get_world_size()

print(f"({rank}) 1:")
print(f"({rank}) rank: {rank}")
print(f"({rank}) size: {size}")

print(f"({rank}) 2:")
tensor = None
if (rank == 0):
    tensor = torch.ones([3,4])
else:
    tensor = torch.zeros([3,4])
if (rank == 0):
    torch.distributed.send(tensor, 1)
if (rank == 1):
    torch.distributed.recv(tensor)
print(f"({rank})", tensor)


# print("3:")
# if (rank == 0):
#     tensor = torch.ones([3,4])
# else:
#     tensor = torch.zeros([3,4])
# handler = None
# if (rank == 0):
#     handle = torch.distributed.isend(tensor, 1)
# if (rank == 1):
#     handle = torch.distributed.irecv(tensor)
# print(f"{rank}:", tensor)


print(f"({rank}) 4:")
tensor = torch.tensor([
    [1,  2,  3,  4],
    [5,  6,  7,  8],
    [9, 10, 11, 12]
    ])

torch.distributed.all_reduce(tensor, op=torch.distributed.ReduceOp.SUM)
print(f"({rank})", tensor)
```

`./slurm_run_simple_dist.sh`
```
#!/usr/bin/env bash
##
#SBATCH --job-name=eml
#SBATCH --output=simple_dist.out
#SBATCH -p short
#SBATCH -N 2
#SBATCH --cpus-per-task=1
#SBATCH --time=00:01:00

# activate virtual environment
module load anaconda3/2024.02-1
conda activate /work/EML/pytorch_env_new
module load mpi/openmpi/4.1.1

mpirun -n 2 --host "localhost:2"  python simple_dist.py
```

`simple_dist.out`
```
Anaconda loaded. Run "conda activate <env>" to activate a conda environment (base, ..).
(0) 1:
(0) rank: 0
(0) size: 2
(0) 2:
(1) 1:
(1) rank: 1
(1) size: 2
(1) 2:
(0) tensor([[1., 1., 1., 1.],
        [1., 1., 1., 1.],
        [1., 1., 1., 1.]])
(0) 4:
(1) tensor([[1., 1., 1., 1.],
        [1., 1., 1., 1.],
        [1., 1., 1., 1.]])
(1) 4:
(0) tensor([[ 2,  4,  6,  8],
        [10, 12, 14, 16],
        [18, 20, 22, 24]])
(1) tensor([[ 2,  4,  6,  8],
        [10, 12, 14, 16],
        [18, 20, 22, 24]])
```

13.2. Vista Point: Data Parallelism

# 1, 2

`dataloader_dist.py`
```python
import torch

torch.distributed.init_process_group(backend="mpi")
rank = torch.distributed.get_rank()
size = torch.distributed.get_world_size()

class SimpleDataSet( torch.utils.data.Dataset ):
    def __init__( self,
        i_length ):
      self.m_length = i_length

    def __len__( self ):
      return self.m_length

    def __getitem__( self,
        i_idx ):
      return i_idx*10

if (rank == 0):
    print(f"world size: {size}")
    print(f"rank: {rank}")
    print()
    print("shuffle=False")
    dataset = SimpleDataSet(10);
    sampler = torch.utils.data.distributed.DistributedSampler(dataset, shuffle=False)
    loader = torch.utils.data.DataLoader(dataset, shuffle=False, sampler=sampler)
    for epoch in range(0, 2):
        sampler.set_epoch(epoch)
        print(f"epoch {epoch}")
        for (l_points) in loader:
            print(l_points)

    print()
    print("shuffle=True")
    sampler = torch.utils.data.distributed.DistributedSampler(dataset, shuffle=True)
    loader = torch.utils.data.DataLoader(dataset, shuffle=False, sampler=sampler)
    for epoch in range(0, 2):
        sampler.set_epoch(epoch)
        print(f"epoch {epoch}")
        for (l_points) in loader:
            print(l_points)

    print()
    print("num_replicas=5")
    sampler = torch.utils.data.distributed.DistributedSampler(dataset, shuffle=False, num_replicas=5)
    loader = torch.utils.data.DataLoader(dataset, shuffle=False, sampler=sampler)
    for (l_points) in loader:
        print(l_points)

    print()
    print("rank=3")
    sampler = torch.utils.data.distributed.DistributedSampler(dataset, shuffle=False, rank=3)
    loader = torch.utils.data.DataLoader(dataset, shuffle=False, sampler=sampler)
    for (l_points) in loader:
        print(l_points)

    print()
    print("drop_last=True")
    sampler = torch.utils.data.distributed.DistributedSampler(dataset, shuffle=False, drop_last=True)
    loader = torch.utils.data.DataLoader(dataset, shuffle=False, sampler=sampler)
    for (l_points) in loader:
        print(l_points)

    sampler = torch.utils.data.distributed.DistributedSampler(dataset, shuffle=False)
    print()
    print("BatchSampler")
    print("batch_size=2")
    batch_sampler = torch.utils.data.BatchSampler(sampler, batch_size=2, drop_last=False)
    for batch in batch_sampler:
        print(batch)

    print()
    print("batch_size=3")
    batch_sampler = torch.utils.data.BatchSampler(sampler, batch_size=3, drop_last=False)
    for batch in batch_sampler:
        print(batch)

    print()
    print("drop_last=True")
    batch_sampler = torch.utils.data.BatchSampler(sampler, batch_size=2, drop_last=True)
    for batch in batch_sampler:
        print(batch)
```

`slurm_run_dataloader_dist.sh`
```
#!/usr/bin/env bash
##
#SBATCH --job-name=eml
#SBATCH --output=dataloader_dist.out
#SBATCH -p short
#SBATCH -N 4
#SBATCH --cpus-per-task=1
#SBATCH --time=00:10:00

# activate virtual environment
module load anaconda3/2024.02-1
conda activate /work/EML/pytorch_env_new
module load mpi/openmpi/4.1.1

mpirun -n 4 --host "localhost:4"  python dataloader_dist.py
```

`dataloader_dist.out`
```
Anaconda loaded. Run "conda activate <env>" to activate a conda environment (base, ..).
world size: 4
rank: 0

shuffle=False
epoch 0
tensor([0])
tensor([40])
tensor([80])
epoch 1
tensor([0])
tensor([40])
tensor([80])

shuffle=True
epoch 0
tensor([40])
tensor([30])
tensor([60])
epoch 1
tensor([50])
tensor([0])
tensor([70])

num_replicas=5
tensor([0])
tensor([50])

rank=3
tensor([30])
tensor([70])
tensor([10])

drop_last=True
tensor([0])
tensor([40])

BatchSampler
batch_size=2
[0, 4]
[8]

batch_size=3
[0, 4, 8]

drop_last=True
[0, 4]
```
