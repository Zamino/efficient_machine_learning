import torch

torch.distributed.init_process_group(backend="mpi")
rank = torch.distributed.get_rank()
size = torch.distributed.get_world_size()

class SimpleDataSet( torch.utils.data.Dataset ):
    def __init__( self,
        i_length ):
      self.m_length = i_length

    def __len__( self ):
      return self.m_length

    def __getitem__( self,
        i_idx ):
      return i_idx*10

if (rank == 0):
    print(f"world size: {size}")
    print(f"rank: {rank}")
    print()
    print("shuffle=False")
    dataset = SimpleDataSet(10);
    sampler = torch.utils.data.distributed.DistributedSampler(dataset, shuffle=False)
    loader = torch.utils.data.DataLoader(dataset, shuffle=False, sampler=sampler)
    for epoch in range(0, 2):
        sampler.set_epoch(epoch)
        print(f"epoch {epoch}")
        for (l_points) in loader:
            print(l_points)

    print()
    print("shuffle=True")
    sampler = torch.utils.data.distributed.DistributedSampler(dataset, shuffle=True)
    loader = torch.utils.data.DataLoader(dataset, shuffle=False, sampler=sampler)
    for epoch in range(0, 2):
        sampler.set_epoch(epoch)
        print(f"epoch {epoch}")
        for (l_points) in loader:
            print(l_points)

    print()
    print("num_replicas=5")
    sampler = torch.utils.data.distributed.DistributedSampler(dataset, shuffle=False, num_replicas=5)
    loader = torch.utils.data.DataLoader(dataset, shuffle=False, sampler=sampler)
    for (l_points) in loader:
        print(l_points)

    print()
    print("rank=3")
    sampler = torch.utils.data.distributed.DistributedSampler(dataset, shuffle=False, rank=3)
    loader = torch.utils.data.DataLoader(dataset, shuffle=False, sampler=sampler)
    for (l_points) in loader:
        print(l_points)

    print()
    print("drop_last=True")
    sampler = torch.utils.data.distributed.DistributedSampler(dataset, shuffle=False, drop_last=True)
    loader = torch.utils.data.DataLoader(dataset, shuffle=False, sampler=sampler)
    for (l_points) in loader:
        print(l_points)

    sampler = torch.utils.data.distributed.DistributedSampler(dataset, shuffle=False)
    print()
    print("BatchSampler")
    print("batch_size=2")
    batch_sampler = torch.utils.data.BatchSampler(sampler, batch_size=2, drop_last=False)
    for batch in batch_sampler:
        print(batch)

    print()
    print("batch_size=3")
    batch_sampler = torch.utils.data.BatchSampler(sampler, batch_size=3, drop_last=False)
    for batch in batch_sampler:
        print(batch)

    print()
    print("drop_last=True")
    batch_sampler = torch.utils.data.BatchSampler(sampler, batch_size=2, drop_last=True)
    for batch in batch_sampler:
        print(batch)
