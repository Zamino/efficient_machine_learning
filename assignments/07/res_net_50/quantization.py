import torch
import torchvision
import time
import pprint
import aimet_common.defs
import aimet_torch.quantsim
from torchvision.io import read_image
from torchvision.models import resnet50, ResNet50_Weights

config = {
        'path_data':       '/mnt/hd1/data/imagenet/ilsvrc_2012',
        'batch_size':      128,
        'num_synthetic':   1,
        'cuda':            True
        }

def calibrate( io_model, i_data, i_use_cuda = False ):
      io_model( i_data )

def get_data():
    trafo = torchvision.transforms.Compose([
        torchvision.transforms.Resize( 256 ),
        torchvision.transforms.CenterCrop( 224 ),
        torchvision.transforms.ToTensor(),
        # move to gpu
        torchvision.transforms.Lambda( lambda x: x.cuda() ),
        # scale to [-1, 1]
        torchvision.transforms.Lambda( lambda x: 2.0 * (x - 0.5) )
        ])

    dataset_val = torchvision.datasets.ImageNet(
            config['path_data'],
            split = 'val',
            transform = trafo
            )

    return torch.utils.data.DataLoader(
            dataset_val,
            batch_size = config['batch_size'],
            shuffle    = False
            )

def eval_model(model, model_name):
    print( f"************{''.join(['*' for _ in range(0, len(model_name))])}****" )
    print( f"*** Running {model_name} ***" )
    print( f"************{''.join(['*' for _ in range(0, len(model_name))])}****" )

    pprint.pprint( config )

    print( 'Setting up data loader' )
    loader_val = get_data()

    #
    # prep model
    #
    print( "Preparing model for execution" )
    model = model.cuda()
    model.eval()
    print( "***********************************************" )
    print( "*** Running synthetic performance benchmark ***" )
    print( "***********************************************" )
    # warm-up
    print( "Warming up.." )
    batch_synth = torch.randn( config['batch_size'], 3, 224, 224, dtype = torch.float32 ).cuda()

    for _ in range( max( config['num_synthetic'] // 10, 1 ) ):
        with torch.no_grad():
            model( batch_synth )

    # benchmark
    print( "Benchmarking.." )
    time_start = time.time()
    for _ in range( config['num_synthetic'] ):
        with torch.no_grad():
            model( batch_synth )
    time_end = time.time()
    duration_batch_synth = time_end - time_start
    duration_batch_synth /= config['num_synthetic']
    print( "  Time per batch: ", duration_batch_synth )
    print( "  Time per sample:", duration_batch_synth / config['batch_size'] )

    print( "*****************************************************" )
    print( "*** Running inference on ImageNet validation data ***" )
    print( "*****************************************************" )
    # inference
    num_samples = 0
    num_top1_correct = 0
    num_top5_correct = 0

    for id, data in enumerate( loader_val ):
        time_start = time.time()

        batch, labels = data
        labels = labels.cuda()

        with torch.no_grad():
            output = model( batch )

        num_samples += len(labels)
        num_top1_correct += (output.argmax(-1) == labels).sum().item()
        num_top5_correct += (output.topk(5, dim=1).indices == labels.unsqueeze(1)).sum().item()

        time_end = time.time()

        duration_batch = time_end - time_start

        if id % 100 == 0:
            print( "  Finished batch / sample:", id, "/", num_samples )
            print( "    Time per batch: ", duration_batch )
            print( "    Time per sample:", duration_batch / len(labels) )

            print( "    Top-1 accuracy: ", num_top1_correct / num_samples )
            print( "    Top-5 accuracy: ", num_top5_correct / num_samples )

    print( "  Samples:", num_samples )
    print( "  Top-1 accuracy:", num_top1_correct / num_samples )
    print( "  Top-5 accuracy:", num_top5_correct / num_samples )

if __name__ == "__main__":

    # prep model
    l_weights = ResNet50_Weights.DEFAULT
    l_model = resnet50(weights=l_weights)
    l_model = l_model.cuda()
    eval_model(l_model, "ResNet50")

    l_dummy_input = torch.randn(config["batch_size"], 3, 224, 224).cuda()
    l_sim = aimet_torch.quantsim.QuantizationSimModel(
            l_model,
            quant_scheme=aimet_common.defs.QuantScheme.post_training_tf,
            dummy_input=l_dummy_input,
            default_param_bw=8,
            default_output_bw=8
            )

    l_loader_val = get_data()
    l_first_batch, _ = next(iter(l_loader_val))

    l_sim.compute_encodings(
            forward_pass_callback = calibrate,
            forward_pass_callback_args = l_first_batch
            )
    print(l_sim)
    eval_model(l_sim.model, "Quantized ResNet50")
    l_sim.export('./', 'quant_ResNet50', l_dummy_input.cpu())
