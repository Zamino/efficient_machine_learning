# Assignment 07

## 10. Quantization

### 10.1. AI Model Efficiency Toolkit

#### 0 Setup

```
$ sudo openconnect -b --useragent 'AnyConnect' --user=jo73xuv@uni-jena.de vpn.sci.uni-jena.de
$ ssh eml_03@murray.inf-ra.uni-jena.de
$ /opt/miniconda3/bin/conda init
(base) $ conda activate /mnt/hd1/conda/aimet/
(/mnt/hd1/conda/aimet) $
```

#### 1

'compute_encoding' is used to get representable output data of the model to
determine a good output encoding. `forward_pass_callback` is the function that
return output data of the original model. The callback function will be called
with arguments given in `forward_pass_callback_args`.

#### 2, 3 and 4

`simple_linear/simple_linear.py`
```python
import torch
import aimet_common.defs
import aimet_torch.quantsim

class SimpleLinear( torch.nn.Module ):
  def __init__( self ):
    super( SimpleLinear, self ).__init__()

    self.m_layer = torch.nn.Linear( in_features = 4,
                                    out_features = 4,
                                    bias = False )

  def forward( self,
               i_input ):
    l_result = self.m_layer( i_input )
    return l_result

def calibrate( io_model,
               i_use_cuda = False ):
  l_data = torch.Tensor( [0.61, -0.93, 0.71, 0.19] )
  io_model( l_data )

if __name__ == "__main__":
  print( 'Running simple linear quantization example' )

  l_model = SimpleLinear()
  
  # freeze model
  l_model = l_model.eval()
  for l_pa in l_model.parameters():
    l_pa.requires_grad = False

  l_w = torch.tensor( [ [ 0.25, -0.32,  1.58,  2.10],
                        [-1.45,  1.82, -0.29,  3.78],
                        [-2.72, -0.12,  2.24, -1.84],
                        [ 1.93,  0.49,  0.00, -3.19] ],
                        requires_grad = False )

  l_model.m_layer.weight = torch.nn.Parameter( l_w,
                                               requires_grad = False )
  print( l_model )

  l_x = torch.tensor( [0.25, 0.17, -0.31, 0.55] )

  print( 'FP32 Result:')
  l_y = l_model( l_x )
  print( l_y )

  l_dummy_input = torch.randn(1, 4)
  l_sim = aimet_torch.quantsim.QuantizationSimModel(
          l_model,
          quant_scheme=aimet_common.defs.QuantScheme.post_training_tf,
          dummy_input=l_dummy_input,
          default_param_bw=4,
          default_output_bw=4
          )

  l_sim.compute_encodings(
          forward_pass_callback = calibrate,
          forward_pass_callback_args = None
          )
  print(l_sim)

  print( 'I4 Result:')
  l_z = l_sim.model( l_x )
  print( l_z )

  l_sim.export('./', 'quant_simple_linear', l_x)

  print( 'finished' )
```

```
2024-05-19 14:43:06,889 - root - INFO - AIMET
Running simple linear quantization example
SimpleLinear(
  (m_layer): Linear(in_features=4, out_features=4, bias=False)
)
FP32 Result:
tensor([ 0.6733,  2.1158, -2.4068, -1.1887])
2024-05-19 14:43:09,122 - Quant - INFO - No config file provided, defaulting to config file at /mnt/hd1/conda/aimet/lib/python3.8/site-packages/aimet_common/quantsim_config/default_config.json
2024-05-19 14:43:09,136 - Quant - INFO - Unsupported op type Squeeze
2024-05-19 14:43:09,136 - Quant - INFO - Unsupported op type Mean
2024-05-19 14:43:09,136 - Quant - INFO - Selecting DefaultOpInstanceConfigGenerator to compute the specialized config. hw_version:default
-------------------------
Quantized Model Report
-------------------------
----------------------------------------------------------
Layer: m_layer
  Input[0]: bw=4, encoding-present=True
    StaticGrid TensorQuantizer:
    quant-scheme:QuantScheme.post_training_tf, round_mode=RoundingMode.ROUND_NEAREST, bitwidth=4, enabled=True
    min:-0.9839999914169312, max=0.6559999942779541, delta=0.10933333237965902, offset=-9.0
  -------
  Param[weight]: bw=4, encoding-present=True
    StaticGrid TensorQuantizer:
    quant-scheme:QuantScheme.post_training_tf, round_mode=RoundingMode.ROUND_NEAREST, bitwidth=4, enabled=True
    min:-4.319999967302595, max=3.7799999713897705, delta=0.5399999959128243, offset=-8.0
  -------
  Output[0]: bw=4, encoding-present=True
    StaticGrid TensorQuantizer:
    quant-scheme:QuantScheme.post_training_tf, round_mode=RoundingMode.ROUND_NEAREST, bitwidth=4, enabled=True
    min:-2.252160135904948, max=1.9706401189168292, delta=0.2815200169881185, offset=-8.0
  -------

I4 Result:
tensor([ 0.5630,  1.9706, -2.2522, -1.1261])
2024-05-19 14:51:27,626 - Utils - INFO - successfully created onnx model with 1/1 node names updated
2024-05-19 14:51:27,627 - Quant - WARNING - number of input quantizers: 1 available for layer: m_layer doesn't match with number of input tensors: 2
2024-05-19 14:51:27,627 - Quant - INFO - Layers excluded from quantization: []
/mnt/hd1/conda/aimet/lib/python3.8/site-packages/aimet_torch/onnx_utils.py:282: FutureWarning: 'torch.onnx._patch_torch._graph_op' is deprecated in version 1.13 and will be removed in version 1.14. Please note 'g.op()' is to be removed from torch.Graph. Please open a GitHub issue if you need this functionality..
  return g.op('aimet_torch::CustomMarker', inp, id_s=identifier, leaf_s=is_leaf, start_s=start)
/mnt/hd1/conda/aimet/lib/python3.8/site-packages/torch/onnx/_patch_torch.py:81: UserWarning: The shape inference of aimet_torch::CustomMarker type is missing, so it may result in wrong shape inference for the exported graph. Please consider adding it in symbolic function. (Triggered internally at ../torch/csrc/jit/passes/onnx/shape_type_inference.cpp:1884.)
  _C._jit_pass_onnx_node_shape_type_inference(
/mnt/hd1/conda/aimet/lib/python3.8/site-packages/torch/onnx/utils.py:687: UserWarning: The shape inference of aimet_torch::CustomMarker type is missing, so it may result in wrong shape inference for the exported graph. Please consider adding it in symbolic function. (Triggered internally at ../torch/csrc/jit/passes/onnx/shape_type_inference.cpp:1884.)
  _C._jit_pass_onnx_graph_shape_type_inference(
/mnt/hd1/conda/aimet/lib/python3.8/site-packages/torch/onnx/utils.py:1178: UserWarning: The shape inference of aimet_torch::CustomMarker type is missing, so it may result in wrong shape inference for the exported graph. Please consider adding it in symbolic function. (Triggered internally at ../torch/csrc/jit/passes/onnx/shape_type_inference.cpp:1884.)
  _C._jit_pass_onnx_graph_shape_type_inference(
finished
```

The `min` and `max` values are the smallest and largest representable number. Converting from float to the quantized number is done by multiplying the float number with `delta`. `offest` indicates how to get from an unsigned quantization (starting from 0) to the used representation, e.q. `-9` indicates the quantized number range from $-9$ to $6$.

### 10.2. Post-Training Quantization

`res_net_50/quantization.py`
```python
import torch
import torchvision
import time
import pprint
import aimet_common.defs
import aimet_torch.quantsim
from torchvision.io import read_image
from torchvision.models import resnet50, ResNet50_Weights

config = {
        'path_data':       '/mnt/hd1/data/imagenet/ilsvrc_2012',
        'batch_size':      128,
        'num_synthetic':   1,
        'cuda':            True
        }

def calibrate( io_model, i_data, i_use_cuda = False ):
      io_model( i_data )

def get_data():
    trafo = torchvision.transforms.Compose([
        torchvision.transforms.Resize( 256 ),
        torchvision.transforms.CenterCrop( 224 ),
        torchvision.transforms.ToTensor(),
        # move to gpu
        torchvision.transforms.Lambda( lambda x: x.cuda() ),
        # scale to [-1, 1]
        torchvision.transforms.Lambda( lambda x: 2.0 * (x - 0.5) )
        ])

    dataset_val = torchvision.datasets.ImageNet(
            config['path_data'],
            split = 'val',
            transform = trafo
            )

    return torch.utils.data.DataLoader(
            dataset_val,
            batch_size = config['batch_size'],
            shuffle    = False
            )

def eval_model(model, model_name):
    print( f"************{''.join(['*' for _ in range(0, len(model_name))])}****" )
    print( f"*** Running {model_name} ***" )
    print( f"************{''.join(['*' for _ in range(0, len(model_name))])}****" )

    pprint.pprint( config )

    print( 'Setting up data loader' )
    loader_val = get_data()

    #
    # prep model
    #
    print( "Preparing model for execution" )
    model = model.cuda()
    model.eval()
    print( "***********************************************" )
    print( "*** Running synthetic performance benchmark ***" )
    print( "***********************************************" )
    # warm-up
    print( "Warming up.." )
    batch_synth = torch.randn( config['batch_size'], 3, 224, 224, dtype = torch.float32 ).cuda()

    for _ in range( max( config['num_synthetic'] // 10, 1 ) ):
        with torch.no_grad():
            model( batch_synth )

    # benchmark
    print( "Benchmarking.." )
    time_start = time.time()
    for _ in range( config['num_synthetic'] ):
        with torch.no_grad():
            model( batch_synth )
    time_end = time.time()
    duration_batch_synth = time_end - time_start
    duration_batch_synth /= config['num_synthetic']
    print( "  Time per batch: ", duration_batch_synth )
    print( "  Time per sample:", duration_batch_synth / config['batch_size'] )

    print( "*****************************************************" )
    print( "*** Running inference on ImageNet validation data ***" )
    print( "*****************************************************" )
    # inference
    num_samples = 0
    num_top1_correct = 0
    num_top5_correct = 0

    for id, data in enumerate( loader_val ):
        time_start = time.time()

        batch, labels = data
        labels = labels.cuda()

        with torch.no_grad():
            output = model( batch )

        num_samples += len(labels)
        num_top1_correct += (output.argmax(-1) == labels).sum().item()
        num_top5_correct += (output.topk(5, dim=1).indices == labels.unsqueeze(1)).sum().item()

        time_end = time.time()

        duration_batch = time_end - time_start

        if id % 100 == 0:
            print( "  Finished batch / sample:", id, "/", num_samples )
            print( "    Time per batch: ", duration_batch )
            print( "    Time per sample:", duration_batch / len(labels) )

            print( "    Top-1 accuracy: ", num_top1_correct / num_samples )
            print( "    Top-5 accuracy: ", num_top5_correct / num_samples )

    print( "  Samples:", num_samples )
    print( "  Top-1 accuracy:", num_top1_correct / num_samples )
    print( "  Top-5 accuracy:", num_top5_correct / num_samples )

if __name__ == "__main__":

    # prep model
    l_weights = ResNet50_Weights.DEFAULT
    l_model = resnet50(weights=l_weights)
    l_model = l_model.cuda()
    eval_model(l_model, "ResNet50")

    l_dummy_input = torch.randn(config["batch_size"], 3, 224, 224).cuda()
    l_sim = aimet_torch.quantsim.QuantizationSimModel(
            l_model,
            quant_scheme=aimet_common.defs.QuantScheme.post_training_tf,
            dummy_input=l_dummy_input,
            default_param_bw=8,
            default_output_bw=8
            )

    l_loader_val = get_data()
    l_first_batch, _ = next(iter(l_loader_val))

    l_sim.compute_encodings(
            forward_pass_callback = calibrate,
            forward_pass_callback_args = l_first_batch
            )
    print(l_sim)
    eval_model(l_sim.model, "Quantized ResNet50")
    l_sim.export('./', 'quant_ResNet50', l_dummy_input.cpu())
```

`res_net_50/output.txt`
```
************************
*** Running ResNet50 ***
************************
{'batch_size': 128,
 'cuda': True,
 'num_synthetic': 1,
 'path_data': '/mnt/hd1/data/imagenet/ilsvrc_2012'}
Setting up data loader
Preparing model for execution
***********************************************
*** Running synthetic performance benchmark ***
***********************************************
Warming up..
Benchmarking..
  Time per batch:  0.0034868717193603516
  Time per sample: 2.7241185307502747e-05
*****************************************************
*** Running inference on ImageNet validation data ***
*****************************************************
  Finished batch / sample: 0 / 128
    Time per batch:  0.09050989151000977
    Time per sample: 0.0007071085274219513
    Top-1 accuracy:  0.96875
    Top-5 accuracy:  0.9765625
  Finished batch / sample: 100 / 12928
    Time per batch:  0.09073400497436523
    Time per sample: 0.0007088594138622284
    Top-1 accuracy:  0.846457301980198
    Top-5 accuracy:  0.9689047029702971
  Finished batch / sample: 200 / 25728
    Time per batch:  0.0910789966583252
    Time per sample: 0.0007115546613931656
    Top-1 accuracy:  0.8236551616915423
    Top-5 accuracy:  0.9600046641791045
  Finished batch / sample: 300 / 38528
    Time per batch:  0.09068179130554199
    Time per sample: 0.0007084514945745468
    Top-1 accuracy:  0.8030523255813954
    Top-5 accuracy:  0.9498806063122923
  Samples: 50000
  Top-1 accuracy: 0.79398
  Top-5 accuracy: 0.94636
...
**********************************
*** Running Quantized ResNet50 ***
**********************************
{'batch_size': 128,
 'cuda': True,
 'num_synthetic': 1,
 'path_data': '/mnt/hd1/data/imagenet/ilsvrc_2012'}
Setting up data loader
Preparing model for execution
***********************************************
*** Running synthetic performance benchmark ***
***********************************************
Warming up..
Benchmarking..
  Time per batch:  0.15574359893798828
  Time per sample: 0.0012167468667030334
*****************************************************
*** Running inference on ImageNet validation data ***
*****************************************************
  Finished batch / sample: 0 / 128
    Time per batch:  0.17240285873413086
    Time per sample: 0.0013468973338603973
    Top-1 accuracy:  0.9609375
    Top-5 accuracy:  0.984375
  Finished batch / sample: 100 / 12928
    Time per batch:  0.17230939865112305
    Time per sample: 0.0013461671769618988
    Top-1 accuracy:  0.838644801980198
    Top-5 accuracy:  0.9672029702970297
  Finished batch / sample: 200 / 25728
    Time per batch:  0.17230772972106934
    Time per sample: 0.0013461541384458542
    Top-1 accuracy:  0.814443407960199
    Top-5 accuracy:  0.9563510572139303
  Finished batch / sample: 300 / 38528
    Time per batch:  0.1722121238708496
    Time per sample: 0.0013454072177410126
    Top-1 accuracy:  0.7951879152823921
    Top-5 accuracy:  0.9455201411960132
  Samples: 50000
  Top-1 accuracy: 0.7857
  Top-5 accuracy: 0.94142
...
```
