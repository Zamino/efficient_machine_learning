import numpy
import torch

l_outputs = []
for i in range(0,10):
  l_outputs.append((
    torch.tensor(numpy.fromfile(f"./output/host_fp32/Result_{i}/class_probs.raw", dtype=numpy.float32)).reshape([32, 1000]),
    torch.tensor(numpy.genfromtxt(f"/opt/data/imagenet/raw_test/batch_size_32/labels_{i}.csv", delimiter=',', dtype=numpy.float32)),
    ))

l_correct = 0
l_total = 0
for l_output, l_labels in l_outputs:
  l_correct += (l_output.argmax(-1) == l_labels).sum().item()
  l_total += l_labels.size()[0]

print(f"The model on the host has a accuracy of {l_correct / l_total}%.")
