# 08

## 11. Mobile Inference

### 11.1. Model and Data Conversions

#### 0 Setup

```bash
conda activate ai_direct
export QNN_SDK_ROOT=/opt/qcom/aistack/qnn/2.18.0.240101/
source ${QNN_SDK_ROOT}/bin/envsetup.sh
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/lib/x86_64-linux-gnu/:/opt/anaconda3/envs/ai_direct/lib/
export PATH=$PATH:/opt/qcom/HexagonSDK/5.5.0.1/tools/android-ndk-r25c
```

#### 1 Host CPU

```bash
# download and extract model
wget https://scalable.uni-jena.de/opt/eml/_downloads/1229246319a97eb6d27f55704761a0d1/aimet_export.tar.xz
tar -xf aimet_export.tar.xz

# produces a network which expects 32,224,224,3 input data (see model/resnet18_fp32.cpp)
${QNN_SDK_ROOT}/bin/x86_64-linux-clang/qnn-onnx-converter \
  --input_network aimet_export/resnet18/resnet18.onnx \
  --input_encoding 'input' other \
  --batch 32 \
  --debug \
  --output model/resnet18_fp32.cpp

# compile a dynamic library which represents the model
${QNN_SDK_ROOT}/bin/x86_64-linux-clang/qnn-model-lib-generator \
  -c model/resnet18_fp32.cpp \
  -b model/resnet18_fp32.bin \
  -o model_libs

# generate list of inputs
touch target_raw_list_host.txt
for i in $(seq 0 9); do echo /opt/data/imagenet/raw_test/batch_size_32/inputs_$i.raw >> target_raw_list_host.txt; done

# run the model
${QNN_SDK_ROOT}/bin/x86_64-linux-clang/qnn-net-run \
              --log_level=info \
              --backend ${QNN_SDK_ROOT}/lib/x86_64-linux-clang/libQnnCpu.so \
              --model model_libs/x86_64-linux-clang/libresnet18_fp32.so \
              --input_list target_raw_list_host.txt \
              --output_dir=output/host_fp32
```

`host_evaluate.py`
```python
import numpy
import torch

l_outputs = []
for i in range(0,10):
  l_outputs.append((
    torch.tensor(numpy.fromfile(f"./output/host_fp32/Result_{i}/class_probs.raw", dtype=numpy.float32)).reshape([32, 1000]),
    torch.tensor(numpy.genfromtxt(f"/opt/data/imagenet/raw_test/batch_size_32/labels_{i}.csv", delimiter=',', dtype=numpy.float32)),
    ))

l_correct = 0
l_total = 0
for l_output, l_labels in l_outputs:
  l_correct += (l_output.argmax(-1) == l_labels).sum().item()
  l_total += l_labels.size()[0]

print(f"The model on the host has a accuracy of {l_correct / l_total}%.")
```

```
The model on the host has a accuracy of 0.665625%.
```

#### 2 Kryo CPU

```bash
# set serial of android device
export ANDROID_SERIAL=2cb92c6a
# set user directory on the device
export DEVICE_USER_DIR=/data/local/tmp/tamino

# create directory to host the model and data on device
adb shell "mkdir -p ${DEVICE_USER_DIR}/resnet18_cpu_fp32"

# copy the runner and compiled model to the device
adb push ${QNN_SDK_ROOT}/bin/aarch64-android/qnn-net-run ${DEVICE_USER_DIR}/resnet18_cpu_fp32
adb push ${QNN_SDK_ROOT}/lib/aarch64-android/libQnnCpu.so ${DEVICE_USER_DIR}/resnet18_cpu_fp32
adb push model_libs/aarch64-android/libresnet18_fp32.so ${DEVICE_USER_DIR}/resnet18_cpu_fp32

# copy data from host to device and set up target list on device
adb shell "mkdir -p ${DEVICE_USER_DIR}/data/imagenet/raw_test/batch_size_32"
adb shell "touch ${DEVICE_USER_DIR}/resnet18_cpu_fp32/target_raw_list.txt"
for batch in $(seq 0 9); do \
  adb shell "echo ${DEVICE_USER_DIR}/data/imagenet/raw_test/batch_size_32/inputs_${batch}.raw >> ${DEVICE_USER_DIR}/resnet18_cpu_fp32/target_raw_list.txt"
  adb push /opt/data/imagenet/raw_test/batch_size_32/inputs_${batch}.raw ${DEVICE_USER_DIR}/data/imagenet/raw_test/batch_size_32/inputs_${batch}.raw 
done

# execute the model on the device CPU
adb shell "cd ${DEVICE_USER_DIR}/resnet18_cpu_fp32; LD_LIBRARY_PATH=. ./qnn-net-run --backend libQnnCpu.so --model libresnet18_fp32.so --input_list target_raw_list.txt"

# copy results from device to host
adb pull ${DEVICE_USER_DIR}/resnet18_cpu_fp32/output output/cpu_fp32
```

`cpu_evaluate.py`
```python
import numpy
import torch

l_outputs = []
for i in range(0,10):
  l_outputs.append((
    torch.tensor(numpy.fromfile(f"./output/cpu_fp32/Result_{i}/class_probs.raw", dtype=numpy.float32)).reshape([32, 1000]),
    torch.tensor(numpy.genfromtxt(f"/opt/data/imagenet/raw_test/batch_size_32/labels_{i}.csv", delimiter=',', dtype=numpy.float32)),
    ))

l_correct = 0
l_total = 0
for l_output, l_labels in l_outputs:
  l_correct += (l_output.argmax(-1) == l_labels).sum().item()
  l_total += l_labels.size()[0]

print(f"The model on the cpu has a accuracy of {l_correct / l_total}%.")
```

```
The model on the cpu has a accuracy of 0.665625%.
```
