#!/usr/bin/env bash
##
# Example Draco job script.
##
#SBATCH --job-name=mlp_training
#SBATCH --output=mlp_training_%j.out
#SBATCH -p short
#SBATCH -N 1
#SBATCH --cpus-per-task=96
#SBATCH --time=01:00:00
#SBATCH --mail-type=all
#SBATCH --mail-user=tamino.steinert@uni-jena.de

echo "submit host:"
echo $SLURM_SUBMIT_HOST
echo "submit dir:"
echo $SLURM_SUBMIT_DIR
echo "nodelist:"
echo $SLURM_JOB_NODELIST

# activate virtual environment
source $HOME/venv_pytorch/bin/activate

# train MLP
cd $HOME/eml_03
export PYTHONUNBUFFERED=TRUE
python mlp_fashion_mnist.py
