import torch
import torchvision
import torchvision.transforms.v2
import eml.mlp.model
import eml.mlp.trainer
import eml.mlp.tester

f_mnist_train = torchvision.datasets.FashionMNIST(
        root = "dataset_fashionMNIST",
        train = True,
        download = True,
        transform=torchvision.transforms.v2.Compose([
            torchvision.transforms.v2.ToImage(),
            torchvision.transforms.v2.ToDtype(torch.float32, scale=True)
        ])
    )
l_data_loader = torch.utils.data.DataLoader(f_mnist_train, batch_size=64)

l_mpl_model = eml.mlp.model.Model()
l_loss_func = torch.nn.CrossEntropyLoss()
l_optimizer = torch.optim.SGD(l_mpl_model.parameters(), lr=0.01)

for epoch in range(100):
    l_loss = eml.mlp.trainer.train(
            l_loss_func,
            l_data_loader,
            l_mpl_model,
            l_optimizer
        )
    if (epoch % 10 == 0):
        print(f"Train {epoch}:\n\tloss: {l_loss}")

l_loss, l_n_correct = eml.mlp.tester.test(
        l_loss_func,
        l_data_loader,
        l_mpl_model
    )
print(f"Test:\n\tloss: {l_loss}\n\tcorrect: {l_n_correct}")
