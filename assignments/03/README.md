# Assignment 03

## 4. Multilayer Perceptron

### 4.1. Datasets and Data Loaders

#### 1
**Deprecated:**

`v2.ToTensor()`
[DEPRECATED] Use v2.Compose([v2.ToImage(), v2.ToDtype(torch.float32, scale=True)]) instead.

`visualization.py`
```python
import torch
import torchvision
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

f_mnist = torchvision.datasets.FashionMNIST(
        root = "dataset_fashionMNIST",
        train = True,
        download = True,
        transform=torchvision.transforms.ToTensor()
        )

print(f"f_mnist.data.size(): {f_mnist.data.size()}")
print(f"f_mnist.targets.size(): {f_mnist.targets.size()}")
print(f"first image: f_mnist.data[0]:\n{f_mnist.data[0]})")
print(f"first_label: f_mnist.targets[0]: {f_mnist.targets[0]})")
```

```
f_mnist.data.size(): torch.Size([60000, 28, 28])
f_mnist.targets.size(): torch.Size([60000])
first image: f_mnist.data[0]:
tensor([[  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
           0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
        [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
           0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
        [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
           0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
        [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   0,
           0,  13,  73,   0,   0,   1,   4,   0,   0,   0,   0,   1,   1,   0],
        [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   3,   0,
          36, 136, 127,  62,  54,   0,   0,   0,   1,   3,   4,   0,   0,   3],
        [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   6,   0,
         102, 204, 176, 134, 144, 123,  23,   0,   0,   0,   0,  12,  10,   0],
        [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
         155, 236, 207, 178, 107, 156, 161, 109,  64,  23,  77, 130,  72,  15],
        [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   0,  69,
         207, 223, 218, 216, 216, 163, 127, 121, 122, 146, 141,  88, 172,  66],
        [  0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   1,   1,   0, 200,
         232, 232, 233, 229, 223, 223, 215, 213, 164, 127, 123, 196, 229,   0],
        [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 183,
         225, 216, 223, 228, 235, 227, 224, 222, 224, 221, 223, 245, 173,   0],
        [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0, 193,
         228, 218, 213, 198, 180, 212, 210, 211, 213, 223, 220, 243, 202,   0],
        [  0,   0,   0,   0,   0,   0,   0,   0,   0,   1,   3,   0,  12, 219,
         220, 212, 218, 192, 169, 227, 208, 218, 224, 212, 226, 197, 209,  52],
        [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   6,   0,  99, 244,
         222, 220, 218, 203, 198, 221, 215, 213, 222, 220, 245, 119, 167,  56],
        [  0,   0,   0,   0,   0,   0,   0,   0,   0,   4,   0,   0,  55, 236,
         228, 230, 228, 240, 232, 213, 218, 223, 234, 217, 217, 209,  92,   0],
        [  0,   0,   1,   4,   6,   7,   2,   0,   0,   0,   0,   0, 237, 226,
         217, 223, 222, 219, 222, 221, 216, 223, 229, 215, 218, 255,  77,   0],
        [  0,   3,   0,   0,   0,   0,   0,   0,   0,  62, 145, 204, 228, 207,
         213, 221, 218, 208, 211, 218, 224, 223, 219, 215, 224, 244, 159,   0],
        [  0,   0,   0,   0,  18,  44,  82, 107, 189, 228, 220, 222, 217, 226,
         200, 205, 211, 230, 224, 234, 176, 188, 250, 248, 233, 238, 215,   0],
        [  0,  57, 187, 208, 224, 221, 224, 208, 204, 214, 208, 209, 200, 159,
         245, 193, 206, 223, 255, 255, 221, 234, 221, 211, 220, 232, 246,   0],
        [  3, 202, 228, 224, 221, 211, 211, 214, 205, 205, 205, 220, 240,  80,
         150, 255, 229, 221, 188, 154, 191, 210, 204, 209, 222, 228, 225,   0],
        [ 98, 233, 198, 210, 222, 229, 229, 234, 249, 220, 194, 215, 217, 241,
          65,  73, 106, 117, 168, 219, 221, 215, 217, 223, 223, 224, 229,  29],
        [ 75, 204, 212, 204, 193, 205, 211, 225, 216, 185, 197, 206, 198, 213,
         240, 195, 227, 245, 239, 223, 218, 212, 209, 222, 220, 221, 230,  67],
        [ 48, 203, 183, 194, 213, 197, 185, 190, 194, 192, 202, 214, 219, 221,
         220, 236, 225, 216, 199, 206, 186, 181, 177, 172, 181, 205, 206, 115],
        [  0, 122, 219, 193, 179, 171, 183, 196, 204, 210, 213, 207, 211, 210,
         200, 196, 194, 191, 195, 191, 198, 192, 176, 156, 167, 177, 210,  92],
        [  0,   0,  74, 189, 212, 191, 175, 172, 175, 181, 185, 188, 189, 188,
         193, 198, 204, 209, 210, 210, 211, 188, 188, 194, 192, 216, 170,   0],
        [  2,   0,   0,   0,  66, 200, 222, 237, 239, 242, 246, 243, 244, 221,
         220, 193, 191, 179, 182, 182, 181, 176, 166, 168,  99,  58,   0,   0],
        [  0,   0,   0,   0,   0,   0,   0,  40,  61,  44,  72,  41,  35,   0,
           0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
        [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
           0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0],
        [  0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
           0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0]],
       dtype=torch.uint8))
first_label: f_mnist.targets[0]: 9)
```

#### 2

```python
with PdfPages('example_images.pdf') as pdf:
    for image_idx in [0, 10, 100]:
        plt.figure(figsize=(4, 4))
        plt.imshow(f_mnist.data[image_idx], cmap='grey')
        plt.title(f"image {image_idx}: {f_mnist.targets[image_idx]}")
        pdf.savefig()
        plt.close()
```

[example_images.pdf](./example_images.pdf)

#### 3

`main.py`
```python
import torch
import torchvision

f_mnist = torchvision.datasets.FashionMNIST(
        root = "dataset_fashionMNIST",
        train = True,
        download = True,
        transform=torchvision.transforms.ToTensor()
        )

l_data_loader = torch.utils.data.DataLoader(f_mnist.data, batch_size=64)
```

### 4.2. Training and Validation

#### 1

`eml/mlp/model.py`
```python
import torch

class Model(torch.nn.Module):                                                                      def __init__(self):
        super(Model, self).__init__()
        self.linear_relu_stack = torch.nn.Sequential(
            torch.nn.Linear(28 * 28, 512),
            torch.nn.ReLU(),
            torch.nn.Linear(512, 512),
            torch.nn.ReLU(),
            torch.nn.Linear(512, 10),
        )
        self.flatten = torch.nn.Flatten()

    def forward(self, x):
        x = self.flatten(x)
        output = self.linear_relu_stack(x)
        return output
```

#### 2 and 3

`eml/mpl/trainer.py`
```python
## Trains the given MLP-model.
#  @param i_loss_func used loss function.
#  @param io_data_loader data loader containing the data to which the model is applied (single epoch).
#  @param io_model model which is trained.
#  @param io_optimizer.
#  @return summed loss over all training samples.
def train( i_loss_func,
           io_data_loader,
           io_model,
           io_optimizer ):
    # switch model to training mode
    io_model.train()

    l_loss_total = 0

    for (l_points, l_labels) in io_data_loader:
        l_predictions = io_model(l_points)
        l_loss = i_loss_func(l_predictions, l_labels)
        l_loss.backward()
        io_optimizer.step()
        io_optimizer.zero_grad()
        l_loss_total += l_loss.item()

    return l_loss_total
```

#### 4

`eml/mlp/tester.py`
```python
import torch

## Tests the model
#  @param i_loss_func used loss function.
#  @param io_data_loader data loader containing the data to which the model is applied.
#  @param io_model model which is tested.
#  @return summed loss over all test samples, number of correctly predicted samples.

def test( i_loss_func,
          io_data_loader,
          io_model ):

    # switch model to evaluation mode
    io_model.eval()

    l_loss_total = 0
    l_n_correct = 0

    with torch.no_grad():
        for (l_points, l_labels) in io_data_loader:
            l_predictions = io_model(l_points)
            l_loss_total += i_loss_func(l_predictions, l_labels).item()
            l_n_correct += (l_predictions.argmax(1) == l_labels).sum().item()

    return l_loss_total, l_n_correct
```

`mlp_fashion_mnist.py`
```python
import torch
import torchvision
import torchvision.transforms.v2
import eml.mlp.model
import eml.mlp.trainer
import eml.mlp.tester

f_mnist_train = torchvision.datasets.FashionMNIST(
        root = "dataset_fashionMNIST",
        train = True,
        download = True,
        transform=torchvision.transforms.v2.Compose([
            torchvision.transforms.v2.ToImage(),
            torchvision.transforms.v2.ToDtype(torch.float32, scale=True)
        ])
    )
l_data_loader = torch.utils.data.DataLoader(f_mnist_train, batch_size=64)

l_mpl_model = eml.mlp.model.Model()
l_loss_func = torch.nn.CrossEntropyLoss()
l_optimizer = torch.optim.SGD(l_mpl_model.parameters(), lr=0.01)

for epoch in range(100):
    l_loss = eml.mlp.trainer.train(
            l_loss_func,
            l_data_loader,
            l_mpl_model,
            l_optimizer
        )
    if (epoch % 10 == 0):
        print(f"Train {epoch}:\n\tloss: {l_loss}")

l_loss, l_n_correct = eml.mlp.tester.test(
        l_loss_func,
        l_data_loader,
        l_mpl_model
    )
print(f"Test:\n\tloss: {l_loss}\n\tcorrect: {l_n_correct}")
```

**Test with epoch set to 1**
```
Train 0:
        loss: 1243.7128802537918
Test:
        loss: 734.7053893208504
        correct: 43043
```

### 4.4 Batch Jobs

#### 1.

**Setup**
```
$ ssh jo73xuv@login1.draco.uni-jena.de
$ salloc -N 1 -p short -t 01:00:00
$ ssh node014
$ module load tools/python/3.8
$ pip install virtualenv
$ export PATH=/home/jo73xuv/.local/bin:${PATH}
$ virtualenv venv_pytorch
$ source venv_pytorch/bin/activate
$ pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cpu
```

`slurm_run_mlp_fashion_mnist.sh`
```bash
#!/usr/bin/env bash
##
# Example Draco job script.
##
#SBATCH --job-name=mlp_training
#SBATCH --output=mlp_training_%j.out
#SBATCH -p short
#SBATCH -N 1
#SBATCH --cpus-per-task=96
#SBATCH --time=01:00:00
#SBATCH --mail-type=all
#SBATCH --mail-user=tamino.steinert@uni-jena.de

echo "submit host:"
echo $SLURM_SUBMIT_HOST
echo "submit dir:"
echo $SLURM_SUBMIT_DIR
echo "nodelist:"
echo $SLURM_JOB_NODELIST

# activate virtual environment
source $HOME/venv_pytorch/bin/activate

# train MLP
cd $HOME/eml_03
export PYTHONUNBUFFERED=TRUE
python mlp_fashion_mnist.py
```

#### 2

**Starting the job:**
```
$ sbatch slurm_run_mlp_fashion_mnist.sh
Submitted batch job 1116325
$ squeue --user=jo73xuv --long
Wed Apr 24 21:27:42 2024
             JOBID PARTITION     NAME     USER    STATE       TIME TIME_LIMI  NODES NODELIST(REASON)
           1116325     short mlp_trai  jo73xuv  PENDING       0:00   1:00:00      1 (Priority)
$ squeue --user=jo73xuv --long
Wed Apr 24 21:43:27 2024
             JOBID PARTITION     NAME     USER    STATE       TIME TIME_LIMI  NODES NODELIST(REASON)
           1116325     short mlp_trai  jo73xuv  RUNNING       0:51   1:00:00      1 node016
```

`mlp_training_1116325.out`
```
submit host:
login1.cluster
submit dir:
/home/jo73xuv/eml_03
nodelist:
node016
Train 0:
	loss: 1225.944381594658
Train 10:
	loss: 382.1587779670954
Train 20:
	loss: 324.56951650977135
Train 30:
	loss: 286.15980633348227
Train 40:
	loss: 255.84820642322302
Train 50:
	loss: 230.41148510575294
Train 60:
	loss: 208.06607488170266
Train 70:
	loss: 187.54874993488193
Train 80:
	loss: 168.67303120717406
Train 90:
	loss: 150.30280041322112
Test:
	loss: 157.8260814025998
	correct: 56332
```
