import torch
import torchvision
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

f_mnist = torchvision.datasets.FashionMNIST(
        root = "dataset_fashionMNIST",
        train = True,
        download = True,
        transform=torchvision.transforms.ToTensor()
        )

print(f"f_mnist.data.size(): {f_mnist.data.size()}")
print(f"f_mnist.targets.size(): {f_mnist.targets.size()}")
print(f"first image: f_mnist.data[0]:\n{f_mnist.data[0]})")
print(f"first_label: f_mnist.targets[0]: {f_mnist.targets[0]})")

with PdfPages('example_images.pdf') as pdf:
    for image_idx in [0, 10, 100]:
        plt.figure(figsize=(4, 4))
        plt.imshow(f_mnist.data[image_idx], cmap='grey')
        plt.title(f"image {image_idx}: {f_mnist.targets[image_idx]}")
        pdf.savefig()
        plt.close()
