#include <cstdlib>
#include <ATen/ATen.h>
#include <iostream>

int main() {
  std::cout << "running the ATen examples" << std::endl;

  float l_data[4*2*3] = {  0.0f,  1.0f,  2.0f, 
                           3.0f,  4.0f,  5.0f,

                           6.0f,  7.0f,  8.0f, 
                           9.0f, 10.0f, 11.0f,
                           
                          12.0f, 13.0f, 14.0f,
                          15.0f, 16.0f, 17.0f,
                          
                          18.0f, 19.0f, 20.0f,
                          21.0f, 22.0f, 23.0f };

  std::cout << "l_data (ptr):" << std::endl << l_data << std::endl;

  auto l_tensor = at::from_blob(l_data, {4,2,3});

  std::cout << "l_tensor:"                << std::endl << l_tensor                  << std::endl;
  std::cout << "l_tensor.data_ptr:"       << std::endl << l_tensor.data_ptr()       << std::endl;
  std::cout << "l_tensor.dtype:"          << std::endl << l_tensor.dtype()          << std::endl;
  std::cout << "l_tensor.sizes:"          << std::endl << l_tensor.sizes()          << std::endl;
  std::cout << "l_tensor.strides:"        << std::endl << l_tensor.strides()        << std::endl;
  std::cout << "l_tensor.storage_offset:" << std::endl << l_tensor.storage_offset() << std::endl;
  std::cout << "l_tensor.device:"  	  << std::endl << l_tensor.device()         << std::endl;
  std::cout << "l_tensor.layout:"  	  << std::endl << l_tensor.layout()         << std::endl;
  std::cout << "l_tensor.is_contiguous:"  << std::endl << l_tensor.is_contiguous()  << std::endl;

  l_tensor[0][0][0] = 1.1;
  std::cout << "l_tensor:"                << std::endl << l_tensor                  << std::endl;
  l_data[0] = 2.2f;
  std::cout << "l_tensor:"                << std::endl << l_tensor                  << std::endl;

  auto l_view = l_tensor.select(1,1);
  std::cout << "l_view:"                << std::endl << l_view                  << std::endl;
  std::cout << "l_view.data_ptr:"       << std::endl << l_view.data_ptr()       << std::endl;
  std::cout << "l_view.sizes:"          << std::endl << l_view.sizes()          << std::endl;
  std::cout << "l_view.strides:"        << std::endl << l_view.strides()        << std::endl;
  std::cout << "l_view.storage_offset:" << std::endl << l_view.storage_offset() << std::endl;
  std::cout << "l_view.layout:"         << std::endl << l_view.layout()         << std::endl;
  std::cout << "l_view.is_contiguous:"  << std::endl << l_view.is_contiguous()  << std::endl;

  auto l_cont = l_view.contiguous();
  std::cout << "l_cont:"                << std::endl << l_cont                  << std::endl;
  std::cout << "l_cont.data_ptr:"       << std::endl << l_cont.data_ptr()       << std::endl;
  std::cout << "l_cont.sizes:"          << std::endl << l_cont.sizes()          << std::endl;
  std::cout << "l_cont.strides:"        << std::endl << l_cont.strides()        << std::endl;
  std::cout << "l_cont.storage_offset:" << std::endl << l_cont.storage_offset() << std::endl;
  std::cout << "l_cont.layout:"         << std::endl << l_cont.layout()         << std::endl;
  std::cout << "l_cont.is_contiguous:"  << std::endl << l_cont.is_contiguous()  << std::endl;

  auto l_A = at::rand({16,4});
  auto l_B = at::rand({4,16});
  std::cout << "l_A:" << std::endl << l_A << std::endl;
  std::cout << "l_B:" << std::endl << l_B << std::endl;

  auto l_C = at::matmul(l_A, l_B);
  std::cout << "l_C:" << std::endl << l_C << std::endl;

  auto l_T0 = at::rand({16,4,2});
  auto l_T1 = at::rand({16,2,4});
  std::cout << "l_T0:" << std::endl << l_T0 << std::endl;
  std::cout << "l_T1:" << std::endl << l_T1 << std::endl;

  auto l_T2 = at::bmm(l_T0, l_T1);
  std::cout << "l_T2:" << std::endl << l_T2 << std::endl;

  std::cout << "finished running ATen examples" << std::endl;

  return EXIT_SUCCESS;
}
