import matplotlib
import matplotlib.pyplot as plt
import numpy
import torch

l_data_points = numpy.loadtxt('data_points.csv', delimiter=',')
l_data_labels = numpy.loadtxt('data_labels.csv', delimiter=',')
l_points = torch.tensor(l_data_points)
l_labels = torch.tensor(l_data_labels)

print(f"first point: {l_points[0]}")
print(f"first label: {l_labels[0]}")

fig = plt.figure()
plt.scatter(l_points[:,0], l_points[:,1], c=l_labels)
plt.savefig("plot_2D.png")

fig = plt.figure()
ax = fig.add_subplot(projection='3d')
ax.scatter(l_points[:,0], l_points[:,1], l_points[:,2], c=l_labels)
plt.savefig("plot_3D.png")
