import torch

class Model(torch.nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        # First fully connected layer
        self.fc1 = torch.nn.Linear(3, 1)
        # Apply sigmoid before output
        self.out = torch.nn.Sigmoid()

    def forward(self, x):
        x = self.fc1(x)
        output = self.out(x)
        return output
