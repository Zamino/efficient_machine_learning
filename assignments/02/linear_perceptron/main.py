import torch
import numpy
import eml.perceptron.model
import eml.perceptron.trainer
import eml.vis.points

def main():
    l_data_points = numpy.loadtxt('data_points.csv', delimiter=',')
    l_data_labels = numpy.loadtxt('data_labels.csv', delimiter=',')
    l_points = torch.tensor(l_data_points, dtype=torch.float)
    l_labels = torch.tensor(l_data_labels, dtype=torch.float)
    l_labels = torch.unsqueeze(l_labels, -1)

    l_dataset = torch.utils.data.TensorDataset(l_points, l_labels)

    l_data_loader = torch.utils.data.DataLoader(l_dataset, batch_size=50)


    linear_model = eml.perceptron.model.Model()
    loss_fn = torch.nn.BCELoss()
    optimizer = torch.optim.SGD(linear_model.parameters(), lr=0.01)

    for epoch in range(200):
        loss = eml.perceptron.trainer.train(loss_fn, l_data_loader, linear_model, optimizer)
        if epoch % 10 == 0:
            fig = eml.vis.points.plot(l_points, linear_model)
            fig.savefig(f"plot_3D_{epoch}.png")

if __name__ == "__main__":
    main()
