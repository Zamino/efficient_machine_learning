# Assignment 02

## 2.2. ATen

### Storage

#### 1

**DONE: listen to podcast**

#### 2

```cpp
  float l_data[4*2*3] = {  0.0f,  1.0f,  2.0f,
                           3.0f,  4.0f,  5.0f,

                           6.0f,  7.0f,  8.0f,
                           9.0f, 10.0f, 11.0f,

                          12.0f, 13.0f, 14.0f,
                          15.0f, 16.0f, 17.0f,

                          18.0f, 19.0f, 20.0f,
                          21.0f, 22.0f, 23.0f };

  std::cout << "l_data (ptr):" << std::endl << l_data << std::endl;
```

```
l_data (ptr):
0x7ffd15125c50
```

```cpp
  auto l_tensor = at::from_blob(l_data, {4,2,3});
```

#### 3

```cpp
  std::cout << "l_tensor:"                << std::endl << l_tensor                  << std::endl;
  std::cout << "l_tensor.data_ptr:"       << std::endl << l_tensor.data_ptr()       << std::endl;
  std::cout << "l_tensor.dtype:"          << std::endl << l_tensor.dtype()          << std::endl;
  std::cout << "l_tensor.sizes:"          << std::endl << l_tensor.sizes()          << std::endl;
  std::cout << "l_tensor.strides:"        << std::endl << l_tensor.strides()        << std::endl;
  std::cout << "l_tensor.storage_offset:" << std::endl << l_tensor.storage_offset() << std::endl;
  std::cout << "l_tensor.device:"         << std::endl << l_tensor.device()         << std::endl;
  std::cout << "l_tensor.layout:"         << std::endl << l_tensor.layout()         << std::endl;
  std::cout << "l_tensor.is_contiguous:"  << std::endl << l_tensor.is_contiguous()  << std::endl;
```

```
l_tensor:
(1,.,.) =
  0  1  2
  3  4  5

(2,.,.) =
   6   7   8
   9  10  11

(3,.,.) =
  12  13  14
  15  16  17

(4,.,.) =
  18  19  20
  21  22  23
[ CPUFloatType{4,2,3} ]
l_tensor.data_ptr:
0x7ffd15125c50
l_tensor.dtype:
float
l_tensor.sizes:
[4, 2, 3]
l_tensor.strides:
[6, 3, 1]
l_tensor.storage_offset:
0
l_tensor.device:
cpu
l_tensor.layout:
Strided
l_tensor.is_contiguous:
1
```

#### 4
Added `-D_GLIBCXX_USE_CXX11_ABI=0` to `OPTIONS` in `Makefile`.

```cpp
  l_tensor[0][0][0] = 1.1;
  std::cout << "l_tensor:"                << std::endl << l_tensor                  << std::endl;
  l_data[0] = 2.2f;
  std::cout << "l_tensor:"                << std::endl << l_tensor                  << std::endl;
```

```
l_tensor:
(1,.,.) =
  1.1000  1.0000  2.0000
  3.0000  4.0000  5.0000

(2,.,.) =
   6   7   8
   9  10  11

(3,.,.) =
  12  13  14
  15  16  17

(4,.,.) =
  18  19  20
  21  22  23
[ CPUFloatType{4,2,3} ]
l_tensor:
(1,.,.) =
  2.2000  1.0000  2.0000
  3.0000  4.0000  5.0000

(2,.,.) =
   6   7   8
   9  10  11

(3,.,.) =
  12  13  14
  15  16  17

(4,.,.) =
  18  19  20
  21  22  23
[ CPUFloatType{4,2,3} ]
```

#### 5

Typo in assignment: it should say **`l_tensor[:,1,:]`** instead of `l_tensor[:,1:]`

```cpp
  auto l_view = l_tensor.select(1,1);
  std::cout << "l_view:"                << std::endl << l_view                  << std::endl;
  std::cout << "l_view.data_ptr:"       << std::endl << l_view.data_ptr()       << std::endl;
  std::cout << "l_view.sizes:"          << std::endl << l_view.sizes()          << std::endl;
  std::cout << "l_view.strides:"        << std::endl << l_view.strides()        << std::endl;
  std::cout << "l_view.storage_offset:" << std::endl << l_view.storage_offset() << std::endl;
  std::cout << "l_view.layout:"         << std::endl << l_view.layout()         << std::endl;
  std::cout << "l_view.is_contiguous:"  << std::endl << l_view.is_contiguous()  << std::endl;
```

```
l_view:
  3   4   5
  9  10  11
 15  16  17
 21  22  23
[ CPUFloatType{4,3} ]
l_view.data_ptr:
0x7ffd15125c5c
l_view.sizes:
[4, 3]
l_view.strides:
[6, 1]
l_view.storage_offset:
3
l_view.layout:
Strided
l_view.is_contiguous:
0
```

#### 6
```cpp
  auto l_cont = l_view.contiguous();
  std::cout << "l_cont:"                << std::endl << l_cont                  << std::endl;
  std::cout << "l_cont.data_ptr:"       << std::endl << l_cont.data_ptr()       << std::endl;
  std::cout << "l_cont.sizes:"          << std::endl << l_cont.sizes()          << std::endl;
  std::cout << "l_cont.strides:"        << std::endl << l_cont.strides()        << std::endl;
  std::cout << "l_cont.storage_offset:" << std::endl << l_cont.storage_offset() << std::endl;
  std::cout << "l_cont.layout:"         << std::endl << l_cont.layout()         << std::endl;
  std::cout << "l_cont.is_contiguous:"  << std::endl << l_cont.is_contiguous()  << std::endl;
```

```
l_cont:
  3   4   5
  9  10  11
 15  16  17
 21  22  23
[ CPUFloatType{4,3} ]
l_cont.data_ptr:
0x556ae078b180
l_cont.sizes:
[4, 3]
l_cont.strides:
[3, 1]
l_cont.storage_offset:
0
l_cont.layout:
Strided
l_cont.is_contiguous:
1
```

### Operations

#### 1
```cpp
  auto l_A = at::rand({16,4});
  auto l_B = at::rand({4,16});
  std::cout << "l_A:" << std::endl << l_A << std::endl;
  std::cout << "l_B:" << std::endl << l_B << std::endl;
```

```
l_A:
 0.7817  0.4893  0.1591  0.2348
 0.0893  0.4984  0.6736  0.1736
 0.3702  0.1100  0.3494  0.7392
 0.2341  0.1402  0.9878  0.6677
 0.9786  0.5978  0.1399  0.3062
 0.9246  0.0374  0.8600  0.5032
 0.5544  0.5157  0.0649  0.9630
 0.6883  0.0542  0.5614  0.9145
 0.2196  0.8866  0.4201  0.9569
 0.9107  0.4034  0.5545  0.0415
 0.1486  0.7021  0.1867  0.2217
 0.0197  0.8637  0.5376  0.8533
 0.7066  0.4413  0.1315  0.3898
 0.0036  0.5786  0.0192  0.1923
 0.6373  0.9279  0.5836  0.9965
 0.4060  0.4682  0.0812  0.8136
[ CPUFloatType{16,4} ]
l_B:
Columns 1 to 10 0.3838  0.9333  0.8586  0.4549  0.9138  0.8994  0.1206  0.7274  0.7419  0.1641
 0.0420  0.3672  0.5028  0.0144  0.2212  0.1968  0.8910  0.0944  0.3753  0.5536
 0.9089  0.2346  0.1230  0.9243  0.8700  0.7547  0.3160  0.7007  0.2177  0.0492
 0.1499  0.3118  0.5913  0.2278  0.1730  0.9983  0.6847  0.2116  0.9605  0.7833

Columns 11 to 16 0.8042  0.4948  0.4959  0.5811  0.6731  0.3800
 0.1052  0.7480  0.1872  0.7625  0.2194  0.7333
 0.9415  0.4633  0.5780  0.4943  0.1071  0.7440
 0.2851  0.5540  0.6900  0.1030  0.7222  0.9814
[ CPUFloatType{4,16} ]
```

#### 2
```cpp
  auto l_C = at::matmul(l_A, l_B);
  std::cout << "l_C:" << std::endl << l_C << std::endl;
```

```
l_C:
Columns 1 to 10 0.5004  1.0197  1.0756  0.5632  1.0016  1.1538  0.7412  0.7760  1.0236  0.5909
 0.6935  0.4785  0.5128  0.7100  0.8079  0.8600  0.7866  0.6208  0.5666  0.4597
 0.5752  0.6984  0.8533  0.6614  0.7945  1.3562  0.7592  0.6810  1.1020  0.7179
 1.0937  0.7099  0.7878  1.1736  1.2199  1.6501  0.9225  1.0170  1.0826  0.6877
 0.5739  1.2612  1.3391  0.6529  1.2012  1.4091  0.9045  0.9311  1.2749  0.7383
 1.2136  1.2353  1.2160  1.3307  1.6885  1.9904  0.7611  1.3852  1.3705  0.6090
 0.4379  1.0223  1.3127  0.5390  0.8438  1.6105  1.2062  0.7013  1.5440  1.1340
 0.9138  1.0791  1.2280  1.0411  1.2876  1.9663  0.9348  1.0927  1.5315  0.8869
 0.6469  0.9274  1.2518  0.7190  0.9278  1.6442  1.6043  0.7403  1.5062  1.2971
 0.8768  1.1411  1.0775  0.9421  1.4111  1.3584  0.6728  1.0979  0.9876  0.4326
 0.2895  0.5094  0.6346  0.3008  0.4919  0.6340  0.8543  0.3522  0.6273  0.5959
 0.6605  0.7277  1.0218  0.7127  0.8244  1.4452  1.5260  0.6532  1.2753  1.1762
 0.4677  0.9739  1.0752  0.5381  0.9252  1.2107  0.7868  0.7303  1.0928  0.6720
 0.0720  0.2803  0.4101  0.0715  0.1812  0.3235  0.6537  0.1114  0.4087  0.4725
 0.9635  1.3832  1.6748  1.0697  1.4678  2.1910  1.7703  1.1710  1.9052  1.4276
 0.3713  0.8236  1.0751  0.4519  0.6860  1.3308  1.0489  0.5686  1.2761  0.9672

Columns 11 to 16 0.8968  0.9566  0.7332  0.9302  0.8201  1.0047
 0.8080  0.8253  0.6468  0.7828  0.3670  1.0710
 0.8490  0.8369  0.9162  0.5479  0.8446  1.2068
 1.3234  1.0483  1.1740  0.8001  0.7763  1.5820
 1.0689  1.1659  0.8894  1.1253  1.0260  1.2149
 1.7006  1.1627  1.3098  1.0428  1.0861  1.5125
 0.8358  1.2237  1.0735  0.8467  1.1888  1.5823
 1.3484  1.1478  1.3069  0.8130  1.1958  1.6164
 0.9382  1.4966  1.1780  1.1099  1.0784  1.9853
 1.3087  1.0322  0.8763  1.1152  0.7909  1.0952
 0.4324  0.8080  0.4660  0.7369  0.4342  0.9278
 0.8562  1.3776  1.0710  1.0237  0.8766  1.8783
 0.8496  0.9566  0.7780  0.8523  0.8680  1.0725
 0.1367  0.5500  0.2539  0.4725  0.2703  0.6287
 1.4437  1.8319  1.5147  1.4691  1.4148  2.3348
 0.6842  1.0395  0.8973  0.7169  0.9723  1.3566
[ CPUFloatType{16,16} ]
```

#### 3
```cpp
  auto l_T0 = at::rand({16,4,2});
  auto l_T1 = at::rand({16,2,4});
  std::cout << "l_T0:" << std::endl << l_T0 << std::endl;
  std::cout << "l_T1:" << std::endl << l_T1 << std::endl;
```

```
l_T0:
(1,.,.) =
  0.6352  0.7661
  0.9746  0.9804
  0.3430  0.9671
  0.1081  0.3325

(2,.,.) =
  0.8324  0.3334
  0.1346  0.3948
  0.6345  0.5479
  0.7731  0.0074

(3,.,.) =
  0.5235  0.7770
  0.4239  0.8899
  0.7139  0.7852
  0.6570  0.5619

(4,.,.) =
  0.3831  0.9120
  0.0270  0.9246
  0.3594  0.9649
  0.1723  0.0067

(5,.,.) =
  0.1277  0.5139
  0.0528  0.7615
  0.5310  0.7169
  0.5841  0.9250

(6,.,.) =
  0.3514  0.1096
  0.9266  0.8409
  0.1216  0.2117
  0.6200  0.0878

(7,.,.) =
  0.8346  0.6009
  0.7133  0.7857
  0.6966  0.5503
  0.2085  0.8392

(8,.,.) =
  0.6117  0.8014
  0.7703  0.4774
  0.7072  0.7234
  0.4215  0.7989

(9,.,.) =
  0.9759  0.7707
  0.3953  0.5247
  0.3085  0.2977
  0.9297  0.5748

(10,.,.) =
  0.9613  0.9611
  0.3560  0.2808
  0.3363  0.7343
  0.7099  0.3909

(11,.,.) =
  0.9348  0.2322
  0.3163  0.1334
  0.9651  0.9119
  0.5197  0.6387

(12,.,.) =
  0.1124  0.8908
  0.3530  0.1310
  0.0642  0.8597
  0.6413  0.0981

(13,.,.) =
  0.2722  0.3635
  0.5623  0.0600
  0.5022  0.7453
  0.2929  0.4958

(14,.,.) =
  0.1561  0.3581
  0.4002  0.1545
  0.5984  0.8475
  0.6400  0.3871

(15,.,.) =
  0.8634  0.3434
  0.9481  0.6073
  0.2122  0.2632
  0.1578  0.7140

(16,.,.) =
  0.7002  0.6962
  0.2925  0.2901
  0.4995  0.1998
  0.1745  0.5869
[ CPUFloatType{16,4,2} ]
l_T1:
(1,.,.) =
  0.1734  0.9597  0.9884  0.0854
  0.4149  0.4044  0.3203  0.4032

(2,.,.) =
  0.3569  0.5575  0.0649  0.3533
  0.2956  0.4937  0.2269  0.2688

(3,.,.) =
  0.5700  0.7770  0.2530  0.9116
  0.0909  0.3025  0.1797  0.4017

(4,.,.) =
  0.0627  0.5049  0.7172  0.3561
  0.5616  0.6322  0.2180  0.7827

(5,.,.) =
  0.6037  0.5391  0.2885  0.1303
  0.0021  0.8987  0.4955  0.9676

(6,.,.) =
  0.8984  0.0955  0.6937  0.1741
  0.5388  0.8956  0.3992  0.8809

(7,.,.) =
  0.9622  0.2480  0.6822  0.8633
  0.1704  0.2469  0.4461  0.4387

(8,.,.) =
  0.9509  0.0006  0.5193  0.6577
  0.0370  0.8297  0.2424  0.9737

(9,.,.) =
  0.0763  0.4215  0.7503  0.0705
  0.3262  0.5342  0.7414  0.1378

(10,.,.) =
  0.4274  0.2764  0.7131  0.8807
  0.7301  0.6831  0.7798  0.3939

(11,.,.) =
  0.5899  0.6402  0.6269  0.0316
  0.2561  0.2974  0.3049  0.7429

(12,.,.) =
  0.7532  0.3174  0.1857  0.5835
  0.3435  0.6715  0.3929  0.8319

(13,.,.) =
  0.4290  0.8195  0.6514  0.1977
  0.9441  0.5627  0.1888  0.8359

(14,.,.) =
  0.1642  0.4391  0.1004  0.2481
  0.3394  0.6905  0.1274  0.6431

(15,.,.) =
  0.9477  0.9223  0.6423  0.9145
  0.3113  0.8468  0.1020  0.2993

(16,.,.) =
  0.4245  0.6339  0.6269  0.9002
  0.4039  0.7785  0.9364  0.5351
[ CPUFloatType{16,2,4} ]
```

#### 4
```cpp
  auto l_T2 = at::bmm(l_T0, l_T1);
  std::cout << "l_T2:" << std::endl << l_T2 << std::endl;
```

```
l_T2:
(1,.,.) =
  0.4281  0.9194  0.8732  0.3631
  0.5759  1.3318  1.2772  0.4785
  0.4608  0.7203  0.6487  0.4192
  0.1567  0.2382  0.2134  0.1433

(2,.,.) =
  0.3956  0.6286  0.1296  0.3837
  0.1647  0.2699  0.0983  0.1537
  0.3884  0.6242  0.1654  0.3715
  0.2781  0.4346  0.0518  0.2752

(3,.,.) =
  0.3690  0.6418  0.2721  0.7894
  0.3225  0.5986  0.2671  0.7439
  0.4783  0.7922  0.3217  0.9662
  0.4256  0.6805  0.2672  0.8247

(4,.,.) =
  0.5362  0.7700  0.4735  0.8502
  0.5209  0.5982  0.2209  0.7333
  0.5644  0.7915  0.4681  0.8832
  0.0146  0.0912  0.1250  0.0666

(5,.,.) =
  0.0782  0.5307  0.2915  0.5138
  0.0335  0.7128  0.3925  0.7437
  0.3221  0.9305  0.5084  0.7629
  0.3546  1.1461  0.6268  0.9711

(6,.,.) =
  0.3747  0.1317  0.2875  0.1577
  1.2855  0.8417  0.9785  0.9021
  0.2233  0.2012  0.1688  0.2076
  0.6042  0.1378  0.4651  0.1852

(7,.,.) =
  0.9054  0.3553  0.8374  0.9841
  0.8202  0.3709  0.8371  0.9605
  0.7640  0.3086  0.7208  0.8428
  0.3436  0.2589  0.5166  0.5482

(8,.,.) =
  0.6113  0.6653  0.5119  1.1827
  0.7502  0.3966  0.5157  0.9716
  0.6992  0.6007  0.5426  1.1695
  0.4304  0.6631  0.4125  1.0552

(9,.,.) =
  0.3258  0.8231  1.3036  0.1750
  0.2013  0.4469  0.6856  0.1002
  0.1206  0.2891  0.4522  0.0628
  0.2584  0.6990  1.1237  0.1447

(10,.,.) =
  1.1126  0.9223  1.4350  1.2252
  0.3572  0.2902  0.4728  0.4242
  0.6798  0.5946  0.8124  0.5854
  0.5888  0.4632  0.8111  0.7792

(11,.,.) =
  0.6109  0.6675  0.6569  0.2020
  0.2207  0.2422  0.2390  0.1091
  0.8028  0.8890  0.8831  0.7079
  0.4701  0.5227  0.5205  0.4909

(12,.,.) =
  0.3906  0.6338  0.3709  0.8066
  0.3109  0.2000  0.1170  0.3150
  0.3436  0.5977  0.3497  0.7526
  0.5167  0.2694  0.1576  0.4558

(13,.,.) =
  0.4599  0.4276  0.2459  0.3576
  0.2979  0.4946  0.3776  0.1613
  0.9191  0.8309  0.4678  0.7223
  0.5937  0.5190  0.2844  0.4723

(14,.,.) =
  0.1472  0.3158  0.0613  0.2690
  0.1181  0.2824  0.0599  0.1986
  0.3859  0.8479  0.1681  0.6935
  0.2365  0.5483  0.1136  0.4077

(15,.,.) =
  0.9251  1.0871  0.5896  0.8923
  1.0876  1.3888  0.6710  1.0488
  0.2830  0.4185  0.1631  0.2728
  0.3718  0.7503  0.1742  0.3581

(16,.,.) =
  0.5785  0.9858  1.0908  1.0028
  0.2413  0.4112  0.4550  0.4185
  0.2928  0.4722  0.5002  0.5565
  0.3111  0.5675  0.6589  0.4711
[ CPUFloatType{16,4,4} ]
```

## 3. Linear Perceptron

### 3.0 Setup

```bash

(base) $ conda create --name eml
(base) $ conda activate eml
(eml) $ conda install matplotlib
(eml) $ conda install pytorch torchvision torchaudio cpuonly -c pytorch
(eml) $ wget https://scalable.uni-jena.de/opt/eml/_downloads/849e2d5f69040cc61dfbc0f75ef74bdb/data_points.csv
(eml) $ wget https://scalable.uni-jena.de/opt/eml/_downloads/b34cecd54e49c08a4ced5566ba920161/data_labels.csv
(eml) $ ls
data_labels.csv  data_points.csv
```

### 3.1 Data Exploration

`data_visualization.py`
```python
import matplotlib.pyplot as plt
import numpy
import torch

l_data_points = numpy.loadtxt('data_points.csv', delimiter=',')
l_data_labels = numpy.loadtxt('data_labels.csv', delimiter=',')
l_points = torch.tensor(l_data_points)
l_labels = torch.tensor(l_data_labels)

print(f"first point: {l_points[0]}")
print(f"first label: {l_labels[0]}")

fig = plt.figure()
plt.scatter(l_points[:,0], l_points[:,1], c=l_labels)
plt.savefig("plot_2D.png")

fig = plt.figure()
ax = fig.add_subplot(projection='3d')
ax.scatter(l_points[:,0], l_points[:,1], l_points[:,2], c=l_labels)
plt.savefig("plot_3D.png")
```

```
first point: tensor([-0.8495, -0.0503,  0.0387], dtype=torch.float64)
first label: 0.0
```

![plot_2D.png](./linear_perceptron/plot_2D.png)
![plot_3D.png](./linear_perceptron/plot_3D.png)

### 3.2. Datasets and Data Loaders

#### 1

```python
import torch
import numpy

l_data_points = numpy.loadtxt('data_points.csv', delimiter=',')
l_data_labels = numpy.loadtxt('data_labels.csv', delimiter=',')
l_points = torch.tensor(l_data_points)
l_labels = torch.tensor(l_data_labels)

l_dataset = torch.utils.data.TensorDataset(l_points, l_labels)
```

#### 2
```python
print(f"len(l_dataset): {len(l_dataset)}")
l_data_loader = torch.utils.data.DataLoader(l_dataset, batch_size=4)
print(f"len(l_data_loader) (batch_size=4): {len(l_data_loader)}")
l_data_loader = torch.utils.data.DataLoader(l_dataset, batch_size=50)
print(f"len(l_data_loader) (batch_size=50): {len(l_data_loader)}")
```

```
len(l_dataset): 1000
len(l_data_loader) (batch_size=4): 250
len(l_data_loader) (batch_size=50): 20
```

### 3.3. Training

#### 1

`eml/perceptron/model.py`
```python
import torch

class Model(torch.nn.Module):
    def __init__(self):
        super(Model, self).__init__()
        # First fully connected layer
        self.fc1 = torch.nn.Linear(3, 1)
        # Apply sigmoid before output
        self.out = torch.nn.Sigmoid()

    def forward(self, x):
        x = self.fc1(x)
        output = self.out(x)
        return output
```

#### 2
`main.py`
```python
import torch
import numpy
import eml.perceptron.model

def main():
    l_data_points = numpy.loadtxt('data_points.csv', delimiter=',')
    l_data_labels = numpy.loadtxt('data_labels.csv', delimiter=',')
    l_points = torch.tensor(l_data_points, dtype=torch.float)
    l_labels = torch.tensor(l_data_labels, dtype=torch.float)
    l_labels = torch.unsqueeze(l_labels, -1)

    l_dataset = torch.utils.data.TensorDataset(l_points, l_labels)
    l_data_loader = torch.utils.data.DataLoader(l_dataset, batch_size=50)

    linear_model = eml.perceptron.model.Model()
    loss_fn = torch.nn.BCELoss()
    optimizer = torch.optim.SGD(linear_model.parameters(), lr=0.01)

    for epoch in range(10):
        total_loss = 0
        for batch in l_data_loader:
            predictions = linear_model(batch[0])
            loss = loss_fn(predictions, batch[1])
            total_loss += loss
        print(total_loss)
        optimizer.zero_grad()
        total_loss.backward()
        optimizer.step()


if __name__ == "__main__":
    main()
```

```
tensor(16.6885, grad_fn=<AddBackward0>)
tensor(16.2367, grad_fn=<AddBackward0>)
tensor(15.8228, grad_fn=<AddBackward0>)
tensor(15.4435, grad_fn=<AddBackward0>)
tensor(15.0955, grad_fn=<AddBackward0>)
tensor(14.7755, grad_fn=<AddBackward0>)
tensor(14.4809, grad_fn=<AddBackward0>)
tensor(14.2088, grad_fn=<AddBackward0>)
tensor(13.9568, grad_fn=<AddBackward0>)
tensor(13.7229, grad_fn=<AddBackward0>)
```

#### 3


`eml/perceptron/trainer.py`
```python
## Trains the given linear perceptron.
#  @param i_loss_func used loss function.
#  @param io_data_loader data loader which provides the training data.
#  @param io_model model which is trained.
#  @param io_optimizer used optimizer.
#  @return loss.
def train( i_loss_func,
           io_data_loader,
           io_model,
           io_optimizer ):
    # switch model to training mode
    io_model.train()

    l_loss_total = 0

    for l_batch in io_data_loader:
        l_predictions = io_model(l_batch[0])
        l_loss = i_loss_func(l_predictions, l_batch[1])
        l_loss_total += l_loss
    io_optimizer.zero_grad()
    l_loss_total.backward()
    io_optimizer.step()

    return l_loss_total
```

`main.py`
```python
import numpy
import eml.perceptron.model
import eml.perceptron.trainer

def main():
    l_data_points = numpy.loadtxt('data_points.csv', delimiter=',')
    l_data_labels = numpy.loadtxt('data_labels.csv', delimiter=',')
    l_points = torch.tensor(l_data_points, dtype=torch.float)
    l_labels = torch.tensor(l_data_labels, dtype=torch.float)
    l_labels = torch.unsqueeze(l_labels, -1)

    l_dataset = torch.utils.data.TensorDataset(l_points, l_labels)

    l_data_loader = torch.utils.data.DataLoader(l_dataset, batch_size=50)


    linear_model = eml.perceptron.model.Model()
    loss_fn = torch.nn.BCELoss()
    optimizer = torch.optim.SGD(linear_model.parameters(), lr=0.01)

    for epoch in range(10000):
        loss = eml.perceptron.trainer.train(loss_fn, l_data_loader, linear_model, optimizer)
        if epoch % 100 == 0:
            print(loss)


if __name__ == "__main__":
    main()
```

```
tensor(13.1179, grad_fn=<AddBackward0>)
tensor(7.3952, grad_fn=<AddBackward0>)
tensor(4.9883, grad_fn=<AddBackward0>)
tensor(3.7014, grad_fn=<AddBackward0>)
tensor(2.9234, grad_fn=<AddBackward0>)
tensor(2.4090, grad_fn=<AddBackward0>)
tensor(2.0460, grad_fn=<AddBackward0>)
tensor(1.7770, grad_fn=<AddBackward0>)
tensor(1.5702, grad_fn=<AddBackward0>)
tensor(1.4064, grad_fn=<AddBackward0>)
tensor(1.2735, grad_fn=<AddBackward0>)
tensor(1.1637, grad_fn=<AddBackward0>)
tensor(1.0714, grad_fn=<AddBackward0>)
tensor(0.9928, grad_fn=<AddBackward0>)
tensor(0.9250, grad_fn=<AddBackward0>)
tensor(0.8660, grad_fn=<AddBackward0>)
tensor(0.8141, grad_fn=<AddBackward0>)
tensor(0.7682, grad_fn=<AddBackward0>)
tensor(0.7272, grad_fn=<AddBackward0>)
tensor(0.6905, grad_fn=<AddBackward0>)
tensor(0.6574, grad_fn=<AddBackward0>)
tensor(0.6273, grad_fn=<AddBackward0>)
tensor(0.5999, grad_fn=<AddBackward0>)
tensor(0.5748, grad_fn=<AddBackward0>)
tensor(0.5518, grad_fn=<AddBackward0>)
tensor(0.5306, grad_fn=<AddBackward0>)
tensor(0.5110, grad_fn=<AddBackward0>)
tensor(0.4928, grad_fn=<AddBackward0>)
tensor(0.4759, grad_fn=<AddBackward0>)
tensor(0.4602, grad_fn=<AddBackward0>)
tensor(0.4454, grad_fn=<AddBackward0>)
tensor(0.4316, grad_fn=<AddBackward0>)
tensor(0.4187, grad_fn=<AddBackward0>)
tensor(0.4065, grad_fn=<AddBackward0>)
tensor(0.3950, grad_fn=<AddBackward0>)
tensor(0.3842, grad_fn=<AddBackward0>)
tensor(0.3740, grad_fn=<AddBackward0>)
tensor(0.3643, grad_fn=<AddBackward0>)
tensor(0.3551, grad_fn=<AddBackward0>)
tensor(0.3463, grad_fn=<AddBackward0>)
tensor(0.3380, grad_fn=<AddBackward0>)
tensor(0.3301, grad_fn=<AddBackward0>)
tensor(0.3226, grad_fn=<AddBackward0>)
tensor(0.3154, grad_fn=<AddBackward0>)
tensor(0.3085, grad_fn=<AddBackward0>)
tensor(0.3020, grad_fn=<AddBackward0>)
tensor(0.2957, grad_fn=<AddBackward0>)
tensor(0.2897, grad_fn=<AddBackward0>)
tensor(0.2839, grad_fn=<AddBackward0>)
tensor(0.2783, grad_fn=<AddBackward0>)
tensor(0.2730, grad_fn=<AddBackward0>)
tensor(0.2679, grad_fn=<AddBackward0>)
tensor(0.2629, grad_fn=<AddBackward0>)
tensor(0.2582, grad_fn=<AddBackward0>)
tensor(0.2536, grad_fn=<AddBackward0>)
tensor(0.2492, grad_fn=<AddBackward0>)
tensor(0.2449, grad_fn=<AddBackward0>)
tensor(0.2408, grad_fn=<AddBackward0>)
tensor(0.2369, grad_fn=<AddBackward0>)
tensor(0.2330, grad_fn=<AddBackward0>)
tensor(0.2293, grad_fn=<AddBackward0>)
tensor(0.2257, grad_fn=<AddBackward0>)
tensor(0.2222, grad_fn=<AddBackward0>)
tensor(0.2189, grad_fn=<AddBackward0>)
tensor(0.2156, grad_fn=<AddBackward0>)
tensor(0.2124, grad_fn=<AddBackward0>)
tensor(0.2094, grad_fn=<AddBackward0>)
tensor(0.2064, grad_fn=<AddBackward0>)
tensor(0.2035, grad_fn=<AddBackward0>)
tensor(0.2007, grad_fn=<AddBackward0>)
tensor(0.1979, grad_fn=<AddBackward0>)
tensor(0.1953, grad_fn=<AddBackward0>)
tensor(0.1927, grad_fn=<AddBackward0>)
tensor(0.1901, grad_fn=<AddBackward0>)
tensor(0.1877, grad_fn=<AddBackward0>)
tensor(0.1853, grad_fn=<AddBackward0>)
tensor(0.1830, grad_fn=<AddBackward0>)
tensor(0.1807, grad_fn=<AddBackward0>)
tensor(0.1785, grad_fn=<AddBackward0>)
tensor(0.1763, grad_fn=<AddBackward0>)
tensor(0.1742, grad_fn=<AddBackward0>)
tensor(0.1722, grad_fn=<AddBackward0>)
tensor(0.1702, grad_fn=<AddBackward0>)
tensor(0.1682, grad_fn=<AddBackward0>)
tensor(0.1663, grad_fn=<AddBackward0>)
tensor(0.1645, grad_fn=<AddBackward0>)
tensor(0.1626, grad_fn=<AddBackward0>)
tensor(0.1608, grad_fn=<AddBackward0>)
tensor(0.1591, grad_fn=<AddBackward0>)
tensor(0.1574, grad_fn=<AddBackward0>)
tensor(0.1557, grad_fn=<AddBackward0>)
tensor(0.1541, grad_fn=<AddBackward0>)
tensor(0.1525, grad_fn=<AddBackward0>)
tensor(0.1509, grad_fn=<AddBackward0>)
tensor(0.1494, grad_fn=<AddBackward0>)
tensor(0.1479, grad_fn=<AddBackward0>)
tensor(0.1464, grad_fn=<AddBackward0>)
tensor(0.1450, grad_fn=<AddBackward0>)
tensor(0.1436, grad_fn=<AddBackward0>)
tensor(0.1422, grad_fn=<AddBackward0>)
```

## 3.4. Visualization

`eml/vis/points.py`
```python
import torch
import matplotlib.pyplot

## Plots the given points and colors them by the predicted labels.
#  It is assumed that a prediction larger than 0.5 corresponds to a red point.
#  All other points are black.
#  @param i_points points in R^3.
#  @param io_model model which is applied to derive the predictions.
def plot( i_points,
          io_model ):
    # switch to evaluation mode
    io_model.eval()

    with torch.no_grad():
        l_predictions = io_model(i_points)
        l_predictions = l_predictions > 0.5
        fig = matplotlib.pyplot.figure()
        ax = fig.add_subplot(projection='3d')
        ax.scatter(i_points[:,0], i_points[:,1], i_points[:,2], c=l_predictions)
        return fig
```

`main.py`
```python
import torch
import numpy
import eml.perceptron.model
import eml.perceptron.trainer
import eml.vis.points

def main():
    l_data_points = numpy.loadtxt('data_points.csv', delimiter=',')
    l_data_labels = numpy.loadtxt('data_labels.csv', delimiter=',')
    l_points = torch.tensor(l_data_points, dtype=torch.float)
    l_labels = torch.tensor(l_data_labels, dtype=torch.float)
    l_labels = torch.unsqueeze(l_labels, -1)

    l_dataset = torch.utils.data.TensorDataset(l_points, l_labels)

    l_data_loader = torch.utils.data.DataLoader(l_dataset, batch_size=50)


    linear_model = eml.perceptron.model.Model()
    loss_fn = torch.nn.BCELoss()
    optimizer = torch.optim.SGD(linear_model.parameters(), lr=0.01)

    for epoch in range(200):
        loss = eml.perceptron.trainer.train(loss_fn, l_data_loader, linear_model, optimizer)
        if epoch % 10 == 0:
            fig = eml.vis.points.plot(l_points, linear_model)
            fig.savefig(f"plot_3D_{epoch}.png")

if __name__ == "__main__":
    main()
```

0:

![plot_3D_0.png](./linear_perceptron/plot_3D_0.png)

10:

![plot_3D_10.png](./linear_perceptron/plot_3D_10.png)

20:

![plot_3D_20.png](./linear_perceptron/plot_3D_20.png)

30:

![plot_3D_30.png](./linear_perceptron/plot_3D_30.png)

40:

![plot_3D_40.png](./linear_perceptron/plot_3D_40.png)

50:

![plot_3D_50.png](./linear_perceptron/plot_3D_50.png)

60:

![plot_3D_60.png](./linear_perceptron/plot_3D_60.png)

70:

![plot_3D_70.png](./linear_perceptron/plot_3D_70.png)

80:

![plot_3D_80.png](./linear_perceptron/plot_3D_80.png)

90:

![plot_3D_90.png](./linear_perceptron/plot_3D_90.png)

100:

![plot_3D_100.png](./linear_perceptron/plot_3D_100.png)

110:

![plot_3D_110.png](./linear_perceptron/plot_3D_110.png)

120:

![plot_3D_120.png](./linear_perceptron/plot_3D_120.png)

130:

![plot_3D_130.png](./linear_perceptron/plot_3D_130.png)

140:

![plot_3D_140.png](./linear_perceptron/plot_3D_140.png)

150:

![plot_3D_150.png](./linear_perceptron/plot_3D_150.png)

160:

![plot_3D_160.png](./linear_perceptron/plot_3D_160.png)

170:

![plot_3D_170.png](./linear_perceptron/plot_3D_170.png)

180:

![plot_3D_180.png](./linear_perceptron/plot_3D_180.png)

190:

![plot_3D_190.png](./linear_perceptron/plot_3D_190.png)
