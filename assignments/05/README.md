# Assignment 5

## 9. GPUs


### 9.1. Offloading Tensors

#### 0 Setup

```
$ sudo openconnect -b --useragent 'AnyConnect' --user=jo73xuv@uni-jena.de vpn.sci.uni-jena.de
$ ssh eml_03@murray.inf-ra.uni-jena.de
...
$ source /mnt/hd1/venvs/pytorch_cuda/bin/activate
(pytorch_cuda) $
```

#### 1

`number_represention.py`
```python
import torch

l_tensor = torch.tensor([-1.0, -0.3, 0.0, 0.4, 1.0])
print("Device: ", l_tensor.device)
l_tensor = l_tensor.cuda()
print("Device: ", l_tensor.device)
relu_func = torch.nn.ReLU()
l_tensor = relu_func(l_tensor)
print(l_tensor)
```

```
Device:  cpu
Device:  cuda:0
tensor([0.0000, 0.0000, 0.0000, 0.4000, 1.0000], device='cuda:0')
```

#### 2

```python
l_t1 = torch.tensor([-1.0, 1.0], device='cuda:0')
l_t2 = torch.tensor([1.0, -1.0], device='cuda:0')
print("l_t1:\n", l_t1)
print("l_t2:\n", l_t2)
print("l_t1 + l_t2:\n", l_t1 + l_t2)
```

```
l_t1:
 tensor([-1.,  1.], device='cuda:0')
l_t2:
 tensor([ 1., -1.], device='cuda:0')
l_t1 + l_t2:
 tensor([0., 0.], device='cuda:0')

```

#### 3

FP16 has only 5 bits to store the exponent. It needs to represent both positive
and negative numbers. Therefore a bias of -15 is added. The largest number with
5 bits is 31, but it's reserved as a special pattern. The next largest number is
30. This means that a fp16 can only store numbers smaller than $2^{16}$.

BP16 has only 7 bits to store the mantissa. So the smallest bit represents the
fraction $2^{-7}$ of the first bit, which is the hidden bit. If a number has a
smaller fraction than this, it is truncated.

The same happens for TF32, except that the mantissa is 10 bits and the smallest
fraction is $2^{-10}$. If a number is stored with an exact fraction of
$2^{-11}$, it is rounded up to the fraction $2^{-10}$. The last bit is only
truncated at a fraction smaller then $2^{-11}$.


```python
print("FP16:")
t_fp32 = torch.tensor([2.0**16], dtype=torch.float32)
print("t_fp32:\n", t_fp32)
t_fp16 = torch.tensor([2.0**16], dtype=torch.float16)
print("t_fp16:\n", t_fp16)
print("BF16:")
t_fp32 = torch.tensor([1+0.5**8], dtype=torch.float32)
print("t_fp32:\n", t_fp32)
t_bf16 = torch.tensor([1+0.5**8], dtype=torch.bfloat16)
print("t_bf16:\n", t_bf16)
print("TF32:")
t_fp32 = torch.ones(100, 100, 100) + (0.5 ** 12)
print("t_fp32:\n", t_fp32)
print("t_fp32 @ t_fp32:\n", t_fp32 @ t_fp32)
torch.backends.cuda.matmul.allow_tf32 = True
t_tf32 = t_fp32.cuda()
print("t_tf32:\n", t_tf32)
print("t_tf32 @ t_tf32:\n", t_tf32 @ t_tf32)
```

```
FP16:
t_fp32:
 tensor([65536.])
t_fp16:
  tensor([inf], dtype=torch.float16)
BF16:
t_fp32:
 tensor([1.0039])
t_bf16:
 tensor([1.], dtype=torch.bfloat16)
TF32:
t_fp32:
 tensor([[[1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         ...,
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002]],

        [[1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         ...,
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002]],

        [[1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         ...,
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002]],

        ...,

        [[1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         ...,
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002]],

        [[1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         ...,
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002]],

        [[1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         ...,
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002]]])
t_fp32 @ t_fp32:
 tensor([[[100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         ...,
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488]],

        [[100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         ...,
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488]],

        [[100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         ...,
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488]],

        ...,

        [[100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         ...,
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488]],

        [[100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         ...,
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488]],

        [[100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         ...,
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488],
         [100.0488, 100.0488, 100.0488,  ..., 100.0488, 100.0488, 100.0488]]])
t_tf32:
 tensor([[[1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         ...,
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002]],

        [[1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         ...,
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002]],

        [[1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         ...,
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002]],

        ...,

        [[1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         ...,
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002]],

        [[1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         ...,
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002]],

        [[1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         ...,
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002],
         [1.0002, 1.0002, 1.0002,  ..., 1.0002, 1.0002, 1.0002]]],
       device='cuda:0')
t_tf32 @ t_tf32:
 tensor([[[100., 100., 100.,  ..., 100., 100., 100.],
         [100., 100., 100.,  ..., 100., 100., 100.],
         [100., 100., 100.,  ..., 100., 100., 100.],
         ...,
         [100., 100., 100.,  ..., 100., 100., 100.],
         [100., 100., 100.,  ..., 100., 100., 100.],
         [100., 100., 100.,  ..., 100., 100., 100.]],

        [[100., 100., 100.,  ..., 100., 100., 100.],
         [100., 100., 100.,  ..., 100., 100., 100.],
         [100., 100., 100.,  ..., 100., 100., 100.],
         ...,
         [100., 100., 100.,  ..., 100., 100., 100.],
         [100., 100., 100.,  ..., 100., 100., 100.],
         [100., 100., 100.,  ..., 100., 100., 100.]],

        [[100., 100., 100.,  ..., 100., 100., 100.],
         [100., 100., 100.,  ..., 100., 100., 100.],
         [100., 100., 100.,  ..., 100., 100., 100.],
         ...,
         [100., 100., 100.,  ..., 100., 100., 100.],
         [100., 100., 100.,  ..., 100., 100., 100.],
         [100., 100., 100.,  ..., 100., 100., 100.]],

        ...,

        [[100., 100., 100.,  ..., 100., 100., 100.],
         [100., 100., 100.,  ..., 100., 100., 100.],
         [100., 100., 100.,  ..., 100., 100., 100.],
         ...,
         [100., 100., 100.,  ..., 100., 100., 100.],
         [100., 100., 100.,  ..., 100., 100., 100.],
         [100., 100., 100.,  ..., 100., 100., 100.]],

        [[100., 100., 100.,  ..., 100., 100., 100.],
         [100., 100., 100.,  ..., 100., 100., 100.],
         [100., 100., 100.,  ..., 100., 100., 100.],
         ...,
         [100., 100., 100.,  ..., 100., 100., 100.],
         [100., 100., 100.,  ..., 100., 100., 100.],
         [100., 100., 100.,  ..., 100., 100., 100.]],

        [[100., 100., 100.,  ..., 100., 100., 100.],
         [100., 100., 100.,  ..., 100., 100., 100.],
         [100., 100., 100.,  ..., 100., 100., 100.],
         ...,
         [100., 100., 100.,  ..., 100., 100., 100.],
         [100., 100., 100.,  ..., 100., 100., 100.],
         [100., 100., 100.,  ..., 100., 100., 100.]]], device='cuda:0')
```

### 9.2. GEMMs

#### 1

```
$ htop
    PID USER      PRI  NI  VIRT   RES   SHR S CPU%▽MEM%   TIME+  Command
1947798 eml_03     20   0 12.4G 3911M  242M R 803.  6.3  7:01.24 python gemms.py
1947892 eml_07     20   0 10.3G  811M  286M R 516.  1.3  3:19.86 python -u mixer.py


$ nvidia-smi
Tue May  7 07:43:55 2024
+-----------------------------------------------------------------------------------------+
| NVIDIA-SMI 550.54.15              Driver Version: 550.54.15      CUDA Version: 12.4     |
|-----------------------------------------+------------------------+----------------------+
| GPU  Name                 Persistence-M | Bus-Id          Disp.A | Volatile Uncorr. ECC |
| Fan  Temp   Perf          Pwr:Usage/Cap |           Memory-Usage | GPU-Util  Compute M. |
|                                         |                        |               MIG M. |
|=========================================+========================+======================|
|   0  NVIDIA GeForce RTX 4070 ...    Off |   00000000:01:00.0 Off |                  N/A |
| 34%   54C    P2            126W /  285W |    1299MiB /  16376MiB |     43%      Default |
|                                         |                        |                  N/A |
+-----------------------------------------+------------------------+----------------------+

+-----------------------------------------------------------------------------------------+
| Processes:                                                                              |
|  GPU   GI   CI        PID   Type   Process name                              GPU Memory |
|        ID   ID                                                               Usage      |
|=========================================================================================|
|    0   N/A  N/A      1553      G   /usr/lib/xorg/Xorg                              4MiB |
|    0   N/A  N/A   1947798      C   python                                        208MiB |
|    0   N/A  N/A   1947892      C   python                                       1074MiB |
+-----------------------------------------------------------------------------------------+
```

```python
import torch
import csv
import time

# open file
csv_file_path = "gemm_time.csv"
with open(csv_file_path, mode='w') as file:
    writer = csv.writer(file)
    writer.writerow(['Device', "dtype", "Size", "GFLOPS", "Time", "Repetitions"])
    for l_device in ["cpu", "cuda:0"]:
        for l_dtype in [torch.float64, torch.float32, torch.float16, torch.bfloat16, "TF32"]:
            for l_size in [512, 1024, 2048, 4096, 8192, 10240]:
                flop_per_iteration = l_size * l_size * l_size * 2; # multiplication + addition;
                l_dtype_string = "";
                if l_dtype == torch.float64:
                    l_dtype_string = "float64"
                if l_dtype == torch.float32:
                    l_dtype_string = "float32"
                if l_dtype == torch.float16:
                    l_dtype_string = "float16"
                if l_dtype == torch.bfloat16:
                    l_dtype_string = "bfloat16"
                if (l_dtype == "TF32"):
                    l_dtype = torch.float32
                    l_dtype_string = "TF32"
                    torch.backends.cuda.matmul.allow_tf32 = True
                else:
                    torch.backends.cuda.matmul.allow_tf32 = False
                l_t1 = torch.ones(l_size, l_size, device=l_device, dtype=l_dtype)
                l_t2 = torch.ones(l_size, l_size, device=l_device, dtype=l_dtype)

                repetitions = 0
                torch.cuda.synchronize()
                time_start = time.perf_counter()
                time_end = time.perf_counter()
                while (time_end - time_start < 10):
                    repetitions += 1
                    l_t3 = l_t1 @ l_t2
                    torch.cuda.synchronize()
                    time_end = time.perf_counter()


                gflops = 1.0E-9 * flop_per_iteration * repetitions / (time_end - time_start)
                print(l_device, l_dtype_string, str(l_size), str(gflops), str(time_end - time_start), repetitions)
                writer.writerow([l_device, l_dtype_string, str(l_size), str(gflops), str(time_end - time_start), repetitions])
```

#### 2

![gemm_plot.pdf](./gemm_plot.png)

### 9.3 MLP-Mixer

#### 1

```
(pytorch_cuda) $ python mixer.py
*************************
*** Running MLP Mixer ***
*************************
{'batch_size': 64,
 'cuda': False,
 'dtype': torch.float32,
 'hidden_size_c': 768,
 'mlp_dim_dc': 3072,
 'mlp_dim_ds': 384,
 'model_impl': 'torchfunc',
 'num_layers': 12,
 'num_synthetic': 1,
 'path_data': '/mnt/hd1/data/imagenet/ilsvrc_2012',
 'path_parameters': '/mnt/hd1/data/mixer_models/imagenet1k/imagenet1k_Mixer-B_16.npz',
 'seq_len_s': 196}
Creating model
Loading parameters
Setting up data loader
Preparing model for execution
***********************************************
*** Running synthetic performance benchmark ***
***********************************************
Warming up..
Benchmarking..
  Time per batch:  6.583280801773071
  Time per sample: 0.10286376252770424
*****************************************************
*** Running inference on ImageNet validation data ***
*****************************************************
  Finished batch / sample: 0 / 64
    Time per batch:  6.388351678848267
    Time per sample: 0.09981799498200417
    Top-1 accuracy:  0.953125
...
  Finished batch / sample: 780 / 49984
    Time per batch:  6.711013555526733
    Time per sample: 0.10485958680510521
    Top-1 accuracy:  0.7648447503201025
    Top-5 accuracy:  0.9212748079385403
  Samples: 50000
  Top-1 accuracy: 0.7648
  Top-5 accuracy: 0.92122
```

#### 2
```python
import torch
import torchvision
import time
import pprint

import models.torchnn
import models.torchfunc

if __name__ == '__main__':
    #
    # config
    #
    # B/16
    config = { 'path_parameters': '/mnt/hd1/data/mixer_models/imagenet1k/imagenet1k_Mixer-B_16.npz',
               'hidden_size_c':   768,
               'seq_len_s':       196,
               'mlp_dim_dc':      3072,
               'mlp_dim_ds':      384,
               'num_layers':      12,
               'path_data':       '/mnt/hd1/data/imagenet/ilsvrc_2012',
               'batch_size':      64,
               'num_synthetic':   1,
               'model_impl':      'torchfunc',
               'dtype':           torch.float32,
               'cuda':            True }

    # L/16
    # config = { 'path_parameters': '/mnt/hd1/data/mixer_models/imagenet1k/imagenet1k_Mixer-L_16.npz',
    #            'hidden_size_c':   1024,
    #            'seq_len_s':       196,
    #            'mlp_dim_dc':      4096,
    #            'mlp_dim_ds':      512,
    #            'num_layers':      24,
    #            'path_data':       '/mnt/hd1/data/imagenet/ilsvrc_2012',
    #            'batch_size':      32,
    #            'num_synthetic':   1,
    #            'model_impl':      'torchnn',
    #            'dtype':           torch.float32,
    #            'cuda':            False }

    print( "*************************" )
    print( "*** Running MLP Mixer ***" )
    print( "*************************" )
    pprint.pprint( config )

    if config["dtype"] == "TF32":
        torch.backends.cuda.matmul.allow_tf32 = True
        config["dtype"] = torch.float32

    #
    # model
    #
    print( 'Creating model' )
    if config['model_impl'] == 'torchnn':
        mixer = models.torchnn.Mixer( hidden_size_c = config['hidden_size_c'],
                                      seq_len_s     = config['seq_len_s'],
                                      mlp_dim_dc    = config['mlp_dim_dc'],
                                      mlp_dim_ds    = config['mlp_dim_ds'],
                                      num_layers    = config['num_layers'] )
    elif config['model_impl'] == 'torchfunc':
        mixer = models.torchfunc.Mixer( num_layers = config['num_layers'] )
    else:
        raise ValueError( "Unknown model implementation" )

    print( 'Loading parameters' )
    mixer.load_parameters(
            config['path_parameters'],
            dtype = config['dtype'],
            cuda = config['cuda']
            )

    #
    # dataloader
    #
    print( 'Setting up data loader' )
    trafo = torchvision.transforms.Compose([
                torchvision.transforms.Resize( 256 ),
                torchvision.transforms.CenterCrop( 224 ),
                torchvision.transforms.ToTensor(),
                # scale to [-1, 1]
                torchvision.transforms.Lambda( lambda x: 2.0 * (x - 0.5) ) ])

    #dataset_val = torchvision.datasets.ImageFolder( config['path_data'],
    #                                                trafo )
    dataset_val = torchvision.datasets.ImageNet( config['path_data'],
                                                 split = 'val',
                                                 transform = trafo )

    loader_val = torch.utils.data.DataLoader( dataset_val,
                                              batch_size = config['batch_size'],
                                              shuffle    = False )

    #
    # prep model
    #
    print( "Preparing model for execution" )
    # prep model
    mixer.eval()

    print( "***********************************************" )
    print( "*** Running synthetic performance benchmark ***" )
    print( "***********************************************" )
    # warm-up
    print( "Warming up.." )
    batch_synth = torch.randn( config['batch_size'], 3, 224, 224, dtype = config['dtype'] )

    for _ in range( max( config['num_synthetic'] // 10, 1 ) ):
        with torch.no_grad():
            mixer( batch_synth )

    # benchmark
    print( "Benchmarking.." )
    time_start = time.time()
    for _ in range( config['num_synthetic'] ):
        with torch.no_grad():
            mixer( batch_synth )
    time_end = time.time()
    duration_batch_synth = time_end - time_start
    duration_batch_synth /= config['num_synthetic']
    print( "  Time per batch: ", duration_batch_synth )
    print( "  Time per sample:", duration_batch_synth / config['batch_size'] )

    print( "*****************************************************" )
    print( "*** Running inference on ImageNet validation data ***" )
    print( "*****************************************************" )
    # inference
    num_samples = 0
    num_top1_correct = 0
    num_top5_correct = 0

    for id, data in enumerate( loader_val ):
        time_start = time.time()

        batch, labels = data

        with torch.no_grad():
            output = mixer( batch )

        num_samples += len(labels)
        num_top1_correct += (output.argmax(-1) == labels).sum().item()
        num_top5_correct += (output.topk(5, dim=1).indices == labels.unsqueeze(1)).sum().item()

        time_end = time.time()

        duration_batch = time_end - time_start

        if id % 10 == 0:
            print( "  Finished batch / sample:", id, "/", num_samples )
            print( "    Time per batch: ", duration_batch )
            print( "    Time per sample:", duration_batch / len(labels) )

            print( "    Top-1 accuracy: ", num_top1_correct / num_samples )
            print( "    Top-5 accuracy: ", num_top5_correct / num_samples )

    print( "  Samples:", num_samples )
    print( "  Top-1 accuracy:", num_top1_correct / num_samples )
    print( "  Top-5 accuracy:", num_top5_correct / num_samples )
```

```python
import torch
import numpy

class MixerLayer():
    def __init__(self):
        self.tensors = {}

    def forward( self,
                 x ):
        tmp = torch.layer_norm( x,
                                normalized_shape = [ x.size()[-1] ],
                                weight = self.tensors['ln1.weight'],
                                bias   = self.tensors['ln1.bias'] )

        # mlp_tokens
        tmp = torch.matmul( self.tensors['mlp_tokens.0.weight'],
                            tmp )
        tmp = torch.add( tmp,
                         self.tensors['mlp_tokens.0.bias'] )
        tmp = torch.nn.functional.gelu( tmp )
        tmp = torch.matmul( self.tensors['mlp_tokens.2.weight'],
                            tmp )
        tmp = torch.add( tmp,
                         self.tensors['mlp_tokens.2.bias'] )
        x = torch.add( x, tmp )

        # mlp_channels
        tmp = torch.layer_norm( x,
                                normalized_shape = [ x.size()[-1] ],
                                weight = self.tensors['ln2.weight'],
                                bias   = self.tensors['ln2.bias'] )

        tmp = torch.matmul( tmp,
                            self.tensors['mlp_channels.0.weight'] )
        tmp = torch.add( tmp,
                         self.tensors['mlp_channels.0.bias'] )
        tmp = torch.nn.functional.gelu( tmp )
        tmp = torch.matmul( tmp,
                            self.tensors['mlp_channels.2.weight'] )
        tmp = torch.add( tmp,
                         self.tensors['mlp_channels.2.bias'] )
        x = torch.add( x, tmp )

        return x

class Mixer():
    def __init__( self,
                  num_layers ):
        self.tensors = {}
        self.mixer_layers = [ MixerLayer() for _ in range( num_layers ) ]
        self.cuda = False

    def load_parameters( self,
                         path_parameters,
                         dtype,
                         cuda
                         ):
        pars = numpy.load( path_parameters )
        self.dtype = dtype
        device = "cpu"
        if cuda:
            self.cuda = True
            device="cuda:0"

        self.tensors['stem.weight'] = torch.tensor( pars['stem/kernel'], device=device, dtype=dtype )
        self.tensors['stem.weight'] = self.tensors['stem.weight'].view( -1, self.tensors['stem.weight'].size()[-1] )
        self.tensors['stem.bias']   = torch.tensor( pars['stem/bias'], device=device, dtype=dtype )

        for la in range( len( self.mixer_layers ) ):
            self.mixer_layers[la].tensors = {
                    'ln1.weight':            torch.tensor( pars[f'MixerBlock_{la}/LayerNorm_0/scale'], device=device, dtype=dtype ),
                    'ln1.bias':              torch.tensor( pars[f'MixerBlock_{la}/LayerNorm_0/bias'], device=device, dtype=dtype ),
                    'ln2.weight':            torch.tensor( pars[f'MixerBlock_{la}/LayerNorm_1/scale'], device=device, dtype=dtype ),
                    'ln2.bias':              torch.tensor( pars[f'MixerBlock_{la}/LayerNorm_1/bias'], device=device, dtype=dtype ),
                    'mlp_tokens.0.weight':   torch.tensor( pars[f'MixerBlock_{la}/token_mixing/Dense_0/kernel'], device=device, dtype=dtype ).transpose( 0, 1 ).unsqueeze(0).contiguous(),
                    'mlp_tokens.0.bias':     torch.tensor( pars[f'MixerBlock_{la}/token_mixing/Dense_0/bias'], device=device, dtype=dtype ).unsqueeze(-1),
                    'mlp_tokens.2.weight':   torch.tensor( pars[f'MixerBlock_{la}/token_mixing/Dense_1/kernel'], device=device, dtype=dtype ).transpose( 0, 1 ).unsqueeze(0).contiguous(),
                    'mlp_tokens.2.bias':     torch.tensor( pars[f'MixerBlock_{la}/token_mixing/Dense_1/bias'], device=device, dtype=dtype ).unsqueeze(-1),
                    'mlp_channels.0.weight': torch.tensor( pars[f'MixerBlock_{la}/channel_mixing/Dense_0/kernel'], device=device, dtype=dtype ),
                    'mlp_channels.0.bias':   torch.tensor( pars[f'MixerBlock_{la}/channel_mixing/Dense_0/bias'], device=device, dtype=dtype ),
                    'mlp_channels.2.weight': torch.tensor( pars[f'MixerBlock_{la}/channel_mixing/Dense_1/kernel'], device=device, dtype=dtype ),
                    'mlp_channels.2.bias':   torch.tensor( pars[f'MixerBlock_{la}/channel_mixing/Dense_1/bias'], device=device, dtype=dtype )
                    }

        self.tensors['ln.weight'] = torch.tensor( pars['pre_head_layer_norm/scale'], device=device, dtype=dtype )
        self.tensors['ln.bias']   = torch.tensor( pars['pre_head_layer_norm/bias'], device=device, dtype=dtype )

        self.tensors['head.weight'] = torch.tensor( pars['head/kernel'], device=device, dtype=dtype )
        self.tensors['head.bias']   = torch.tensor( pars['head/bias'], device=device, dtype=dtype )

    def eval( self ):
        pass

    def forward( self, x ):
        if self.cuda:
            x = x.cuda()
        x = x.to(self.dtype)
        #           0          1          2              3   4              5
        x = x.view( x.size(0), x.size(1), x.size(2)//16, 16, x.size(3)//16, 16 )
        x = x.permute( 0, 2, 4, 3, 5, 1 ).contiguous()
        x = x.view( x.size(0), x.size(1)*x.size(2), -1 )

        x = torch.matmul( x, self.tensors['stem.weight'] )
        x = torch.add( x, self.tensors['stem.bias'] )

        for mixer_layer in self.mixer_layers:
            x = mixer_layer.forward( x )

        x = torch.layer_norm( x,
                              normalized_shape = [ x.size()[-1] ],
                              weight = self.tensors['ln.weight'],
                              bias   = self.tensors['ln.bias'] )
        x = torch.mean( x, dim = 1 )
        x = torch.matmul( x, self.tensors['head.weight'] )
        x = torch.add( x, self.tensors['head.bias'] )

        return x.cpu()

    def __call__( self, x ):
        return self.forward( x )
```

```
(pytorch_cuda) $ python mixer.py
*************************
*** Running MLP Mixer ***
*************************
{'batch_size': 64,
 'cuda': True,
 'dtype': torch.float32,
 'hidden_size_c': 768,
 'mlp_dim_dc': 3072,
 'mlp_dim_ds': 384,
 'model_impl': 'torchfunc',
 'num_layers': 12,
 'num_synthetic': 1,
 'path_data': '/mnt/hd1/data/imagenet/ilsvrc_2012',
 'path_parameters': '/mnt/hd1/data/mixer_models/imagenet1k/imagenet1k_Mixer-B_16.npz',
 'seq_len_s': 196}
Creating model
Loading parameters
Setting up data loader
Preparing model for execution
***********************************************
*** Running synthetic performance benchmark ***
***********************************************
Warming up..
Benchmarking..
  Time per batch:  0.08750462532043457
  Time per sample: 0.0013672597706317902
*****************************************************
*** Running inference on ImageNet validation data ***
*****************************************************
  Finished batch / sample: 0 / 64
    Time per batch:  0.08686304092407227
    Time per sample: 0.0013572350144386292
    Top-1 accuracy:  0.953125
    Top-5 accuracy:  0.96875
...
  Finished batch / sample: 780 / 49984
    Time per batch:  0.09018206596374512
    Time per sample: 0.0014090947806835175
    Top-1 accuracy:  0.7648447503201025
    Top-5 accuracy:  0.9212748079385403
  Samples: 50000
  Top-1 accuracy: 0.7648
  Top-5 accuracy: 0.92122
```

#### 3

```
(pytorch_cuda) $ python mixer.py
*************************
*** Running MLP Mixer ***
*************************
{'batch_size': 128,
 'cuda': True,
 'dtype': torch.float32,
 'hidden_size_c': 768,
 'mlp_dim_dc': 3072,
 'mlp_dim_ds': 384,
 'model_impl': 'torchfunc',
 'num_layers': 12,
 'num_synthetic': 1,
 'path_data': '/mnt/hd1/data/imagenet/ilsvrc_2012',
 'path_parameters': '/mnt/hd1/data/mixer_models/imagenet1k/imagenet1k_Mixer-B_16.npz',
 'seq_len_s': 196}
Creating model
Loading parameters
Setting up data loader
Preparing model for execution
***********************************************
*** Running synthetic performance benchmark ***
***********************************************
Warming up..
Benchmarking..
  Time per batch:  0.17221975326538086
  Time per sample: 0.001345466822385788
*****************************************************
*** Running inference on ImageNet validation data ***
*****************************************************
  Finished batch / sample: 0 / 128
    Time per batch:  0.1723341941833496
    Time per sample: 0.0013463608920574188
    Top-1 accuracy:  0.9609375
    Top-5 accuracy:  0.9765625
...
    Time per batch:  0.11518478393554688
    Time per sample: 0.001439809799194336
    Top-1 accuracy:  0.7648
    Top-5 accuracy:  0.92122
  Samples: 50000
  Top-1 accuracy: 0.7648
  Top-5 accuracy: 0.92122
```

```
(pytorch_cuda) $ python mixer.py
*************************
*** Running MLP Mixer ***
*************************
{'batch_size': 128,
 'cuda': True,
 'dtype': torch.bfloat16,
 'hidden_size_c': 768,
 'mlp_dim_dc': 3072,
 'mlp_dim_ds': 384,
 'model_impl': 'torchfunc',
 'num_layers': 12,
 'num_synthetic': 1,
 'path_data': '/mnt/hd1/data/imagenet/ilsvrc_2012',
 'path_parameters': '/mnt/hd1/data/mixer_models/imagenet1k/imagenet1k_Mixer-B_16.npz',
 'seq_len_s': 196}
Creating model
Loading parameters
Setting up data loader
Preparing model for execution
***********************************************
*** Running synthetic performance benchmark ***
***********************************************
Warming up..
Benchmarking..
  Time per batch:  0.07041049003601074
  Time per sample: 0.0005500819534063339
*****************************************************
*** Running inference on ImageNet validation data ***
*****************************************************
  Finished batch / sample: 0 / 128
    Time per batch:  0.07330060005187988
    Time per sample: 0.0005726609379053116
    Top-1 accuracy:  0.9609375
    Top-5 accuracy:  0.9765625
...
  Finished batch / sample: 390 / 50000
    Time per batch:  0.052068471908569336
    Time per sample: 0.0006508558988571167
    Top-1 accuracy:  0.76464
    Top-5 accuracy:  0.9214
  Samples: 50000
  Top-1 accuracy: 0.76464
  Top-5 accuracy: 0.9214
```

```
(pytorch_cuda) $ python mixer.py
*************************
*** Running MLP Mixer ***
*************************
{'batch_size': 128,
 'cuda': True,
 'dtype': torch.float16,
 'hidden_size_c': 768,
 'mlp_dim_dc': 3072,
 'mlp_dim_ds': 384,
 'model_impl': 'torchfunc',
 'num_layers': 12,
 'num_synthetic': 1,
 'path_data': '/mnt/hd1/data/imagenet/ilsvrc_2012',
 'path_parameters': '/mnt/hd1/data/mixer_models/imagenet1k/imagenet1k_Mixer-B_16.npz',
 'seq_len_s': 196}
Creating model
Loading parameters
Setting up data loader
Preparing model for execution
***********************************************
*** Running synthetic performance benchmark ***
***********************************************
Warming up..
Benchmarking..
  Time per batch:  0.07245302200317383
  Time per sample: 0.0005660392343997955
*****************************************************
*** Running inference on ImageNet validation data ***
*****************************************************
  Finished batch / sample: 0 / 128
    Time per batch:  0.07320308685302734
    Time per sample: 0.0005718991160392761
    Top-1 accuracy:  0.9609375
    Top-5 accuracy:  0.9765625
...
  Finished batch / sample: 390 / 50000
    Time per batch:  0.062410593032836914
    Time per sample: 0.0007801324129104615
    Top-1 accuracy:  0.76498
    Top-5 accuracy:  0.92112
  Samples: 50000
  Top-1 accuracy: 0.76498
  Top-5 accuracy: 0.92112
```

```
(pytorch_cuda) $ python mixer.py
*************************
*** Running MLP Mixer ***
*************************
{'batch_size': 128,
 'cuda': True,
 'dtype': 'TF32',
 'hidden_size_c': 768,
 'mlp_dim_dc': 3072,
 'mlp_dim_ds': 384,
 'model_impl': 'torchfunc',
 'num_layers': 12,
 'num_synthetic': 1,
 'path_data': '/mnt/hd1/data/imagenet/ilsvrc_2012',
 'path_parameters': '/mnt/hd1/data/mixer_models/imagenet1k/imagenet1k_Mixer-B_16.npz',
 'seq_len_s': 196}
Creating model
Loading parameters
Setting up data loader
Preparing model for execution
***********************************************
*** Running synthetic performance benchmark ***
***********************************************
Warming up..
Benchmarking..
  Time per batch:  0.28314995765686035
  Time per sample: 0.0022121090441942215
*****************************************************
*** Running inference on ImageNet validation data ***
*****************************************************
  Finished batch / sample: 0 / 128
    Time per batch:  0.28260254859924316
    Time per sample: 0.0022078324109315872
    Top-1 accuracy:  0.9609375
    Top-5 accuracy:  0.9765625
  Finished batch / sample: 10 / 1408
    Time per batch:  0.15487194061279297
    Time per sample: 0.001209937036037445
    Top-1 accuracy:  0.8913352272727273
    Top-5 accuracy:  0.9723011363636364
...
  Finished batch / sample: 380 / 48768
    Time per batch:  0.14328789710998535
    Time per sample: 0.0011194366961717606
    Top-1 accuracy:  0.7641076115485564
    Top-5 accuracy:  0.9206241797900262
  Finished batch / sample: 390 / 50000
    Time per batch:  0.09328246116638184
    Time per sample: 0.001166030764579773
    Top-1 accuracy:  0.76484
    Top-5 accuracy:  0.92124
  Samples: 50000
  Top-1 accuracy: 0.76484
  Top-5 accuracy: 0.92124
```

Doubling the batch size from 64 to 128 had no impact on the time per sample or
accuracy.

The fastest floating point format is bfloat16. The next fastest format is
`float16`, followed by `TF32` and then `float32`. The accuracies are nearly
identical, with `float16` having the best Top-1 accuracy of `0.76498`.

### 6.3 C++ Extensions

#### 0 Setup

```bash
git clone https://github.com/pybind/pybind11.git
cd pybind11
git submodule add -b stable ../../pybind/pybind11 extern/pybind11
git submodule update --init
conda activate base
pip install pybind11
```

#### 1

`./hello_from_cpp.cpp`
```cpp
#include </home/tamino/git/pybind11/extern/pybind11/include/pybind11/pybind11.h>
#include <iostream>

namespace py = pybind11;

void hello() {
  std::cout << "Hello" << std::endl;
}

PYBIND11_MODULE(example, m) {
  m.doc() = "pybind11 hello";
  m.def("hello", &hello, "Print hello");
}
```

```bash
$ c++ -O3 -Wall -shared -std=c++11 -fPIC $(python3 -m pybind11 --includes) hello_from_cpp.cpp -o example$(python3-config --extension-suffix)
```

```python
import example
example.hello()
Hello
```
