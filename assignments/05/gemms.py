import torch
import csv
import time

csv_file_path = "gemm_time.csv"
with open(csv_file_path, mode='w') as file:
    writer = csv.writer(file)
    writer.writerow(['Device', "dtype", "Size", "GFLOPS", "Time", "Repetitions"])
    for l_device in ["cpu", "cuda:0"]:
        for l_dtype in [torch.float64, torch.float32, torch.float16, torch.bfloat16, "TF32"]:
            for l_size in [512, 1024, 2048, 4096, 8192, 10240]:
                flop_per_iteration = l_size * l_size * l_size * 2; # multiplication + addition;
                l_dtype_string = "";
                if l_dtype == torch.float64:
                    l_dtype_string = "float64"
                    dtype = l_dtype
                if l_dtype == torch.float32:
                    l_dtype_string = "float32"
                    dtype = l_dtype
                if l_dtype == torch.float16:
                    l_dtype_string = "float16"
                    dtype = l_dtype
                    if l_device == "cpu" and l_size == 10240:
                        break
                if l_dtype == torch.bfloat16:
                    l_dtype_string = "bfloat16"
                    dtype = l_dtype
                if (l_dtype == "TF32"):
                    l_dtype_string = "TF32"
                    torch.backends.cuda.matmul.allow_tf32 = True
                    dtype = torch.float32
                else:
                    torch.backends.cuda.matmul.allow_tf32 = False
                l_t1 = torch.ones(l_size, l_size, device=l_device, dtype=dtype)
                l_t2 = torch.ones(l_size, l_size, device=l_device, dtype=dtype)

                repetitions = 0
                torch.cuda.synchronize()
                time_start = time.perf_counter()
                time_end = time.perf_counter()
                while (time_end - time_start < 10):
                    repetitions += 1
                    l_t3 = l_t1 @ l_t2
                    torch.cuda.synchronize()
                    time_end = time.perf_counter()

                gflops = 1.0E-9 * flop_per_iteration * repetitions / (time_end - time_start)
                print(l_device, l_dtype_string, str(l_size), str(gflops), str(time_end - time_start), repetitions)
                writer.writerow([l_device, l_dtype_string, str(l_size), str(gflops), str(time_end - time_start), repetitions])
