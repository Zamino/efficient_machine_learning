#include </home/tamino/git/pybind11/extern/pybind11/include/pybind11/pybind11.h>
#include <iostream>

namespace py = pybind11;

void hello() {
  std::cout << "Hello" << std::endl;
}

PYBIND11_MODULE(example, m) {
  m.doc() = "pybind11 hello";
  m.def("hello", &hello, "Print hello");
}
