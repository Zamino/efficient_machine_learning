import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import pandas
import torch

l_data = pandas.read_csv('gemm_time.csv', delimiter=',')

l_data['Color'] = ["r" if dev == "cpu" else "b" for dev in l_data.iloc[:,0]]

marker_fp64 = mlines.Line2D([],[],marker='*', color='black', linestyle='None', markersize=7, label='FP64')
marker_fp32 = mlines.Line2D([],[],marker='o', color='black', linestyle='None', markersize=7, label='FP32')
marker_fp16 = mlines.Line2D([],[],marker='v', color='black', linestyle='None', markersize=7, label='FP16')
marker_bf16 = mlines.Line2D([],[],marker='X', color='black', linestyle='None', markersize=7, label='BF16')
marker_tf32 = mlines.Line2D([],[],marker='P', color='black', linestyle='None', markersize=7, label='TF32')
marker_cpu = mlines.Line2D([],[],marker='s', color='r', linestyle='None', markersize=7, label='CPU')
marker_gpu = mlines.Line2D([],[],marker='s', color='b', linestyle='None', markersize=7, label='GPU')
markers = [marker_fp64, marker_fp32, marker_fp16, marker_bf16, marker_tf32, marker_cpu, marker_gpu]

l_data['Marker'] = [
    "*" if dtype == "float64" else
    "o" if dtype == "float32" else
    "v" if dtype == "float16" else
    "X" if dtype == "bfloat16" else
    "P" if dtype == "TF32" else
    dtype
    for dtype in l_data.iloc[:,1]]

l_devices = ["cpu", "cuda:0"]
l_dtypes = ["float64", "float32", "float16", "bfloat16", "TF32"]
for device in l_devices:
    l_data_device = l_data
    for dtype in l_dtypes:
        accessor = (l_data.iloc[:,0] == device) * (l_data.iloc[:,1] == dtype) == 1
        l_view = l_data.loc[accessor,:]
        if l_view.size:
            plt.plot(l_view.iloc[:,2], l_view.iloc[:,3], marker=l_view.iloc[0,7], c=l_view.iloc[0,6])

plt.xscale("log", base=2)
# plt.xticks([512, 1024, 2048, 4096, 8192, 10240])
plt.xlabel("Size (log)")
plt.yscale("log", base=2)
plt.ylabel("GFLOPS (log)")
plt.legend(handles=markers)
plt.savefig("gemm_plot.png")
plt.savefig("gemm_plot.pdf")
