import torch

l_tensor = torch.tensor([-1.0, -0.3, 0.0, 0.4, 1.0])
print("Device: ", l_tensor.device)
l_tensor = l_tensor.cuda()
print("Device: ", l_tensor.device)
relu_func = torch.nn.ReLU()
l_tensor = relu_func(l_tensor)
print(l_tensor)

l_t1 = torch.tensor([-1.0, 1.0], device='cuda:0')
l_t2 = torch.tensor([1.0, -1.0], device='cuda:0')
print("l_t1:\n", l_t1)
print("l_t2:\n", l_t2)
print("l_t1 + l_t2:\n", l_t1 + l_t2)

print("FP16:")
t_fp32 = torch.tensor([2.0**(2**4)], dtype=torch.float32)
print("t_fp32:\n", t_fp32)
t_fp16 = torch.tensor([2.0**(2**4)], dtype=torch.float16)
print("t_fp16:\n", t_fp16)
print("BF16:")
t_fp32 = torch.tensor([1+0.5**8], dtype=torch.float32)
print("t_fp32:\n", t_fp32)
t_bf16 = torch.tensor([1+0.5**8], dtype=torch.bfloat16)
print("t_bf16:\n", t_bf16)
print("TF32:")
t_fp32 = torch.ones(100, 100, 100) + (0.5 ** 11)
print("t_fp32:\n", t_fp32)
print("t_fp32 @ t_fp32:\n", t_fp32 @ t_fp32)
torch.backends.cuda.matmul.allow_tf32 = True
t_tf32 = t_fp32.cuda()
print("t_tf32:\n", t_tf32)
print("t_tf32 @ t_tf32:\n", t_tf32 @ t_tf32)



