# Assignment 11

## 12. MLIR

### 12.1. Getting Started

#### 1.

`func_add.mlir`
```
func.func @add( %lhs: tensor<3x2xf32>,
                %rhs: tensor<1x1xf32> ) -> tensor<3x2xf32> {
  %out = tosa.add %lhs, %rhs : (tensor<3x2xf32>, tensor<1x1xf32>) -> tensor<3x2xf32>
  return %out : tensor<3x2xf32>
}
```

```bash
$ export PATH=/opt/iree/tools/:$PATH
$ export LD_LIBRARY_PATH=/opt/iree/lib:$LD_LIBRARY_PATH
$ iree-compile --mlir-print-ir-after-all --iree-hal-target-backends=llvm-cpu func_add.mlir -o func_add.vmfb 2>&1 | tee iree_out.log
...
$ iree-run-module --module=func_add.vmfb --function=add --input="3x2xf32=[[0,1],[2,3],[4,5]]" --input="1x1xf32=[[17]]"
EXEC @add
result[0]: hal.buffer_view
3x2xf32=[17 18][19 20][21 22]
...
```

`iree_out.log`

TODO: Explain

#### 2.

```bash
$ iree-run-module --module=func_add.vmfb --function=add --input="3x2xf32=[[0,1],[2,3],[4,5]]" --input="1x1xf32=[[17]]"
EXEC @add
result[0]: hal.buffer_view
3x2xf32=[17 18][19 20][21 22]
```

#### 1.
```bash
$ iree-opt --pass-pipeline="builtin.module(func.func(tosa-to-linalg))" func_add.mlir -o func_add.linalg
```

`func_add.linalg`
```
#map = affine_map<(d0, d1) -> (d0, d1)>
#map1 = affine_map<(d0, d1) -> (0, 0)>
module {
  func.func @add(%arg0: tensor<3x2xf32>, %arg1: tensor<1x1xf32>) -> tensor<3x2xf32> {
    %0 = tensor.empty() : tensor<3x2xf32>
    %1 = linalg.generic {indexing_maps = [#map, #map1, #map], iterator_types = ["parallel", "parallel"]} ins(%arg0, %arg1 : tensor<3x2xf32>, tensor<1x1xf32>) outs(%0 : tensor<3x2xf32>) {
    ^bb0(%in: f32, %in_0: f32, %out: f32):
      %2 = arith.addf %in, %in_0 : f32
      linalg.yield %2 : f32
    } -> tensor<3x2xf32>
    return %1 : tensor<3x2xf32>
  }
}
```

#### 2.

```bash
$ iree-opt --pass-pipeline="builtin.module(func.func(iree-codegen-iree-comprehensive-bufferize))" func_add.linalg -o func_add.bufferize
```

`func_add.bufferize`
```
#map = affine_map<(d0, d1) -> (d0, d1)>
#map1 = affine_map<(d0, d1) -> (0, 0)>
module {
  func.func @add(%arg0: tensor<3x2xf32>, %arg1: tensor<1x1xf32>) -> tensor<3x2xf32> {
    %0 = bufferization.to_memref %arg1 : memref<1x1xf32, strided<[?, ?], offset: ?>>
    %1 = bufferization.to_memref %arg0 : memref<3x2xf32, strided<[?, ?], offset: ?>>
    %alloc = memref.alloc() : memref<3x2xf32>
    linalg.generic {indexing_maps = [#map, #map1, #map], iterator_types = ["parallel", "parallel"]} ins(%1, %0 : memref<3x2xf32, strided<[?, ?], offset: ?>>, memref<1x1xf32, strided<[?, ?], offset: ?>>) outs(%alloc : memref<3x2xf32>) {
    ^bb0(%in: f32, %in_0: f32, %out: f32):
      %3 = arith.addf %in, %in_0 : f32
      linalg.yield %3 : f32
    }
    %2 = bufferization.to_tensor %alloc : memref<3x2xf32>
    return %2 : tensor<3x2xf32>
  }
}

```

#### 3.

```bash
$ iree-opt --mlir-print-ir-after-all --pass-pipeline="builtin.module(func.func(convert-linalg-to-parallel-loops))" func_add.linalg -o func_add.par_loop
// -----// IR Dump After ConvertLinalgToParallelLoopsPass (convert-linalg-to-parallel-loops) //----- //
func.func @add(%arg0: tensor<3x2xf32>, %arg1: tensor<1x1xf32>) -> tensor<3x2xf32> {
  %0 = tensor.empty() : tensor<3x2xf32>
  %1 = linalg.generic {indexing_maps = [affine_map<(d0, d1) -> (d0, d1)>, affine_map<(d0, d1) -> (0, 0)>, affine_map<(d0, d1) -> (d0, d1)>], iterator_types = ["parallel", "parallel"]} ins(%arg0, %arg1 : tensor<3x2xf32>, tensor<1x1xf32>) outs(%0 : tensor<3x2xf32>) {
  ^bb0(%in: f32, %in_0: f32, %out: f32):
    %2 = arith.addf %in, %in_0 : f32
    linalg.yield %2 : f32
  } -> tensor<3x2xf32>
  return %1 : tensor<3x2xf32>
}
```

`func_add.par_loop`
```
#map = affine_map<(d0, d1) -> (d0, d1)>
#map1 = affine_map<(d0, d1) -> (0, 0)>
module {
  func.func @add(%arg0: tensor<3x2xf32>, %arg1: tensor<1x1xf32>) -> tensor<3x2xf32> {
    %0 = tensor.empty() : tensor<3x2xf32>
    %1 = linalg.generic {indexing_maps = [#map, #map1, #map], iterator_types = ["parallel", "parallel"]} ins(%arg0, %arg1 : tensor<3x2xf32>, tensor<1x1xf32>) outs(%0 : tensor<3x2xf32>) {
    ^bb0(%in: f32, %in_0: f32, %out: f32):
      %2 = arith.addf %in, %in_0 : f32
      linalg.yield %2 : f32
    } -> tensor<3x2xf32>
    return %1 : tensor<3x2xf32>
  }
}

```

### 12.2. Matrix Multiplication

#### 1.

`matmul_gen.mlir`
```
func.func @matmul(%lhs: tensor<8x8xf32>, %rhs: tensor<8x8xf32>) -> tensor<8x8xf32> {
  %init = tensor.empty() : tensor<8x8xf32>
  %result = linalg.generic {
    indexing_maps = [
      affine_map<(i, j, k) -> (i, k)>,
      affine_map<(i, j, k) -> (k, j)>,
      affine_map<(i, j, k) -> (i, j)>
      ],
    iterator_types = ["parallel", "parallel", "reduction"]
  } ins (%lhs, %rhs : tensor<8x8xf32>, tensor<8x8xf32>)
    outs(%init : tensor<8x8xf32>) {
  ^bb0(%lhs_one: f32, %rhs_one: f32, %init_one: f32):
    %0 = arith.mulf %lhs_one, %rhs_one : f32
    %1 = arith.addf %init_one, %0 : f32
    linalg.yield %1 : f32
  } -> tensor<8x8xf32>
  return %result : tensor<8x8xf32>
}
```

```bash
$ iree-compile --iree-hal-target-backends=llvm-cpu --iree-llvmcpu-target-cpu=host matmul_gen.mlir -o matmul_gen.vmfb
$ iree-run-module --module=matmul_gen.vmfb --function=matmul --input="8x8xf32=[ \
>   [ 0,  1,  2,  3,  4,  5,  6,  7], \
>   [ 8,  9, 10, 11, 12, 13, 14, 15], \
>   [16, 17, 18, 19, 20, 21, 22, 23], \
>   [24, 25, 26, 27, 28, 29, 30, 31], \
>   [32, 33, 34, 35, 36, 37, 38, 39], \
>   [40, 41, 42, 43, 44, 45, 46, 47], \
>   [48, 49, 50, 51, 52, 53, 54, 55], \
>   [56, 57, 58, 59, 60, 61, 62, 63] \
> ]" --input="8x8xf32=[ \
>   [ 0,  1,  2,  3,  4,  5,  6,  7], \
>   [ 8,  9, 10, 11, 12, 13, 14, 15], \
>   [16, 17, 18, 19, 20, 21, 22, 23], \
>   [24, 25, 26, 27, 28, 29, 30, 31], \
>   [32, 33, 34, 35, 36, 37, 38, 39], \
>   [40, 41, 42, 43, 44, 45, 46, 47], \
>   [48, 49, 50, 51, 52, 53, 54, 55], \
>   [56, 57, 58, 59, 60, 61, 62, 63] \
> ]"
EXEC @matmul
result[0]: hal.buffer_view
8x8xf32=[1120 1148 1176 1204 1232 1260 1288 1316][2912 3004 3096 3188 3280 3372 3464 3556][4704 4860 5016 5172 5328 5484 5640 5796][6496 6716 6936 7156 7376 7596 7816 8036][8288 8572 8856 9140 9424 9708 9992 10276][10080 10428 10776 11124 11472 11820 12168 12516][11872 12284 12696 13108 13520 13932 14344 14756][13664 14140 14616 15092 15568 16044 16520 16996]
```

#### 2.

`matmul_mat.linalg`
```
func.func @matmul(%lhs: tensor<8x8xf32>, %rhs: tensor<8x8xf32>) -> tensor<8x8xf32> {
  %init = tensor.empty() : tensor<8x8xf32>
  %result = linalg.matmul ins (%lhs, %rhs : tensor<8x8xf32>, tensor<8x8xf32>) outs(%init : tensor<8x8xf32>) -> tensor<8x8xf32>
  return %result : tensor<8x8xf32>
}
```

```bash
$ iree-opt --pass-pipeline="builtin.module(func.func(linalg-generalize-named-ops))" matmul_mat.linalg -o matmul_mat.mlir
```

`matmul_mat.mlir`
```
#map = affine_map<(d0, d1, d2) -> (d0, d2)>
#map1 = affine_map<(d0, d1, d2) -> (d2, d1)>
#map2 = affine_map<(d0, d1, d2) -> (d0, d1)>
module {
  func.func @matmul(%arg0: tensor<8x8xf32>, %arg1: tensor<8x8xf32>) -> tensor<8x8xf32> {
    %0 = tensor.empty() : tensor<8x8xf32>
    %1 = linalg.generic {indexing_maps = [#map, #map1, #map2], iterator_types = ["parallel", "parallel", "reduction"]} ins(%arg0, %arg1 : tensor<8x8xf32>, tensor<8x8xf32>) outs(%0 : tensor<8x8xf32>) {
    ^bb0(%in: f32, %in_0: f32, %out: f32):
      %2 = arith.mulf %in, %in_0 : f32
      %3 = arith.addf %out, %2 : f32
      linalg.yield %3 : f32
    } -> tensor<8x8xf32>
    return %1 : tensor<8x8xf32>
  }
}
```

```bash
$ iree-compile --iree-hal-target-backends=llvm-cpu --iree-llvmcpu-target-cpu=host matmul_mat.linalg -o matmul_mat.vmfb
$ iree-run-module --module=matmul_mat.vmfb --function=matmul --input="8x8xf32=[ \
>   [ 0,  1,  2,  3,  4,  5,  6,  7], \
>   [ 8,  9, 10, 11, 12, 13, 14, 15], \
>   [16, 17, 18, 19, 20, 21, 22, 23], \
>   [24, 25, 26, 27, 28, 29, 30, 31], \
>   [32, 33, 34, 35, 36, 37, 38, 39], \
>   [40, 41, 42, 43, 44, 45, 46, 47], \
>   [48, 49, 50, 51, 52, 53, 54, 55], \
>   [56, 57, 58, 59, 60, 61, 62, 63] \
> ]" --input="8x8xf32=[ \
>   [ 0,  1,  2,  3,  4,  5,  6,  7], \
>   [ 8,  9, 10, 11, 12, 13, 14, 15], \
>   [16, 17, 18, 19, 20, 21, 22, 23], \
>   [24, 25, 26, 27, 28, 29, 30, 31], \
>   [32, 33, 34, 35, 36, 37, 38, 39], \
>   [40, 41, 42, 43, 44, 45, 46, 47], \
>   [48, 49, 50, 51, 52, 53, 54, 55], \
>   [56, 57, 58, 59, 60, 61, 62, 63] \
> ]"
EXEC @matmul
result[0]: hal.buffer_view
8x8xf32=[1120 1148 1176 1204 1232 1260 1288 1316][2912 3004 3096 3188 3280 3372 3464 3556][4704 4860 5016 5172 5328 5484 5640 5796][6496 6716 6936 7156 7376 7596 7816 8036][8288 8572 8856 9140 9424 9708 9992 10276][10080 10428 10776 11124 11472 11820 12168 12516][11872 12284 12696 13108 13520 13932 14344 14756][13664 14140 14616 15092 15568 16044 16520 16996]
```

#### 3.

`matmul_tosa.mlir`
```
func.func @matmul( %lhs: tensor<1x8x8xf32>, %rhs: tensor<1x8x8xf32> ) -> tensor<1x8x8xf32> {
  %out = tosa.matmul %lhs, %rhs : (tensor<1x8x8xf32>, tensor<1x8x8xf32>) -> tensor<1x8x8xf32>
  return %out : tensor<1x8x8xf32>
}
```

```bash
$ iree-compile --iree-hal-target-backends=llvm-cpu --iree-llvmcpu-target-cpu=host matmul_tosa.mlir -o matmul_tosa.vmfb
$ iree-run-module --module=matmul_tosa.vmfb --function=matmul --input="1x8x8xf32=[ \
>   [ 0,  1,  2,  3,  4,  5,  6,  7], \
>   [ 8,  9, 10, 11, 12, 13, 14, 15], \
>   [16, 17, 18, 19, 20, 21, 22, 23], \
>   [24, 25, 26, 27, 28, 29, 30, 31], \
>   [32, 33, 34, 35, 36, 37, 38, 39], \
>   [40, 41, 42, 43, 44, 45, 46, 47], \
>   [48, 49, 50, 51, 52, 53, 54, 55], \
>   [56, 57, 58, 59, 60, 61, 62, 63] \
> ]" --input="1x8x8xf32=[ \
>   [ 0,  1,  2,  3,  4,  5,  6,  7], \
>   [ 8,  9, 10, 11, 12, 13, 14, 15], \
>   [16, 17, 18, 19, 20, 21, 22, 23], \
>   [24, 25, 26, 27, 28, 29, 30, 31], \
>   [32, 33, 34, 35, 36, 37, 38, 39], \
>   [40, 41, 42, 43, 44, 45, 46, 47], \
>   [48, 49, 50, 51, 52, 53, 54, 55], \
>   [56, 57, 58, 59, 60, 61, 62, 63] \
> ]"
EXEC @matmul
result[0]: hal.buffer_view
1x8x8xf32=[[1120 1148 1176 1204 1232 1260 1288 1316][2912 3004 3096 3188 3280 3372 3464 3556][4704 4860 5016 5172 5328 5484 5640 5796][6496 6716 6936 7156 7376 7596 7816 8036][8288 8572 8856 9140 9424 9708 9992 10276][10080 10428 10776 11124 11472 11820 12168 12516][11872 12284 12696 13108 13520 13932 14344 14756][13664 14140 14616 15092 15568 16044 16520 16996]]
[eml_03@grace 11]$
```

#### 4.

`matmul_bench.mlir`
```
func.func @matmul( %lhs: tensor<1x8192x8192xf32>, %rhs: tensor<1x8192x8192xf32> ) -> tensor<1x8192x8192xf32> {
  %out = tosa.matmul %lhs, %rhs : (tensor<1x8192x8192xf32>, tensor<1x8192x8192xf32>) -> tensor<1x8192x8192xf32>
  return %out : tensor<1x8192x8192xf32>
}
```

```bash
$ iree-compile --iree-hal-target-backends=llvm-cpu --iree-llvmcpu-target-cpu=host matmul_bench.mlir -o matmul_bench.vmfb
$ iree-benchmark-module \
>   --module=matmul_bench.vmfb \
>   --device=local-task \
>   --function=matmul \
>   --input=1x8192x8192xf32 \
>   --input=1x8192x8192xf32 \
>   --task_topology_max_group_count=72
2024-06-19T18:00:24+02:00
Running iree-benchmark-module
Run on (144 X 3321 MHz CPU s)
CPU Caches:
  L1 Data 64 KiB (x144)
  L1 Instruction 64 KiB (x144)
  L2 Unified 1024 KiB (x144)
  L3 Unified 116736 KiB (x2)
Load Average: 133.58, 111.01, 66.17
-------------------------------------------------------------------------------------------
Benchmark                                 Time             CPU   Iterations UserCounters...
-------------------------------------------------------------------------------------------
BM_matmul/process_time/real_time        695 ms        42959 ms            1 items_per_second=1.43984/s
$ iree-benchmark-module \
>   --module=matmul_bench.vmfb \
>   --device=local-task \
>   --function=matmul \
>   --input=1x8192x8192xf32 \
>   --input=1x8192x8192xf32 \
>   --task_topology_max_group_count=36
2024-06-19T18:00:50+02:00
Running iree-benchmark-module
Run on (144 X 3321 MHz CPU s)
CPU Caches:
  L1 Data 64 KiB (x144)
  L1 Instruction 64 KiB (x144)
  L2 Unified 1024 KiB (x144)
  L3 Unified 116736 KiB (x2)
Load Average: 125.58, 111.25, 67.46
-------------------------------------------------------------------------------------------
Benchmark                                 Time             CPU   Iterations UserCounters...
-------------------------------------------------------------------------------------------
BM_matmul/process_time/real_time       1210 ms        40565 ms            1 items_per_second=0.826614/s
$ iree-benchmark-module \
>   --module=matmul_bench.vmfb \
>   --device=local-task \
>   --function=matmul \
>   --input=1x8192x8192xf32 \
>   --input=1x8192x8192xf32 \
>   --task_topology_max_group_count=18
2024-06-19T18:01:09+02:00
Running iree-benchmark-module
Run on (144 X 3321 MHz CPU s)
CPU Caches:
  L1 Data 64 KiB (x144)
  L1 Instruction 64 KiB (x144)
  L2 Unified 1024 KiB (x144)
  L3 Unified 116736 KiB (x2)
Load Average: 104.05, 107.08, 67.01
-------------------------------------------------------------------------------------------
Benchmark                                 Time             CPU   Iterations UserCounters...
-------------------------------------------------------------------------------------------
BM_matmul/process_time/real_time       2574 ms        40751 ms            1 items_per_second=0.388506/s
$ iree-benchmark-module \
>   --module=matmul_bench.vmfb \
>   --device=local-task \
>   --function=matmul \
>   --input=1x8192x8192xf32 \
>   --input=1x8192x8192xf32 \
>   --task_topology_max_group_count=18
2024-06-19T18:01:27+02:00
Running iree-benchmark-module
Run on (144 X 3321 MHz CPU s)
CPU Caches:
  L1 Data 64 KiB (x144)
  L1 Instruction 64 KiB (x144)
  L2 Unified 1024 KiB (x144)
  L3 Unified 116736 KiB (x2)
Load Average: 98.24, 105.57, 67.16
-------------------------------------------------------------------------------------------
Benchmark                                 Time             CPU   Iterations UserCounters...
-------------------------------------------------------------------------------------------
BM_matmul/process_time/real_time        679 ms        42153 ms            1 items_per_second=1.47186/s
```

#### 5.

TODO: Explain
