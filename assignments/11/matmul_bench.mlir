func.func @matmul( %lhs: tensor<1x8192x8192xf32>, %rhs: tensor<1x8192x8192xf32> ) -> tensor<1x8192x8192xf32> {
  %out = tosa.matmul %lhs, %rhs : (tensor<1x8192x8192xf32>, tensor<1x8192x8192xf32>) -> tensor<1x8192x8192xf32>
  return %out : tensor<1x8192x8192xf32>
}
