func.func @matmul( %lhs: tensor<1x8x8xf32>, %rhs: tensor<1x8x8xf32> ) -> tensor<1x8x8xf32> {
  %out = tosa.matmul %lhs, %rhs : (tensor<1x8x8xf32>, tensor<1x8x8xf32>) -> tensor<1x8x8xf32>
  return %out : tensor<1x8x8xf32>
}
