import unittest
import math
from . import node

class TestAutograd(unittest.TestCase):
  def test_f(self):
    l_x = node.Node(1.0)
    l_y = node.Node(2.0)
    l_z = node.Node(3.0)
    l_result = l_x * (l_y + l_z)
    l_result.backward(1.0)
    self.assertEqual(l_x.m_value*(l_y.m_value+l_z.m_value), l_result.m_value)
    self.assertEqual((5, 1, 1), (l_x.m_grad, l_y.m_grad, l_z.m_grad))

  def test_g(self):
    l_w0 = node.Node(1.0)
    l_w1 = node.Node(2.0)
    l_w2 = node.Node(0.0)
    l_x0 = node.Node(2.0)
    l_x1 = node.Node(-1.0)
    l_result = node.Node(1.0)/(
        node.Node(1.0) + (
          node.Node(-1.0)*(l_w0*l_x0+l_w1*l_x1+l_w2)
        ).exp()
      )
    l_result.backward(1.0)

    self.assertEqual(
      1 / (
        1+math.exp(
          -1*(l_w0.m_value*l_x0.m_value+l_w1.m_value*l_x1.m_value+l_w2.m_value)
        )
      ),
      l_result.m_value )
    self.assertEqual((0.5, -0.25, 0.25, 0.25, 0.5), (l_w0.m_grad,l_w1.m_grad,l_w2.m_grad,l_x0.m_grad,l_x1.m_grad))

  def test_h(self):
    l_x = node.Node(0.0)
    l_y = node.Node(0.0)
    l_result = ((l_x * l_y).sin() + (l_x + l_y).cos()) / (l_x - l_y).exp()
    l_result.backward(1.0)
    self.assertEqual(1, l_result.m_value)
    self.assertEqual((-1.0, 1.0), (l_x.m_grad, l_y.m_grad))
