import unittest
from . import Mul
from .. import context

class TestMul( unittest.TestCase ):
  def test_forward( self ):
    l_result = Mul.forward(context.Context(), 3.0, 4.0)

    self.assertAlmostEqual(l_result, 12.0)

  def test_backward( self ):
    l_context = context.Context()
    l_context.save_for_backward(3.0, 4.0)
    l_grad_a, l_grad_b = Mul.backward(l_context, 5.0)

    self.assertEqual( l_grad_a, 20.0 )
    self.assertEqual( l_grad_b, 15.0 )
