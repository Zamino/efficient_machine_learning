import unittest
from . import Sub

class TestSub( unittest.TestCase ):
  def test_forward( self ):
    l_result = Sub.forward( None,
                            3.0,
                            4.0 )

    self.assertAlmostEqual( l_result,
                            -1.0 )

  def test_backward( self ):
    l_grad_a, l_grad_b = Sub.backward( None,
                                       5.0 )

    self.assertEqual( l_grad_a, 5.0 )
    self.assertEqual( l_grad_b, -5.0 )
