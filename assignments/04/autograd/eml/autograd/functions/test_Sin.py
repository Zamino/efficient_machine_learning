import unittest
from . import Sin
from .. import context

class TestSin( unittest.TestCase ):
  def test_forward( self ):
    l_result = Sin.forward(context.Context(), 0.0)

    self.assertAlmostEqual(l_result, 0.0)

  def test_backward( self ):
    l_context = context.Context()
    l_context.save_for_backward(0.0)
    [l_grad_a] = Sin.backward(l_context, 5.0)

    self.assertEqual( l_grad_a, 5.0 )
