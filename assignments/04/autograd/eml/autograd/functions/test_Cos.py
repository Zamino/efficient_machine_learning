import unittest
from . import Cos
from .. import context

class TestCos( unittest.TestCase ):
  def test_forward( self ):
    l_result = Cos.forward(context.Context(), 0.0)

    self.assertAlmostEqual(l_result, 1.0)

  def test_backward( self ):
    l_context = context.Context()
    l_context.save_for_backward(0.0)
    [l_grad_a] = Cos.backward(l_context, 5.0)

    self.assertEqual( l_grad_a, 0.0 )
