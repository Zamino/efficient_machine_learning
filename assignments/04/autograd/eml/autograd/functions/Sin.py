import math

## Forward method which compute sin(a).
# @param i_ctx context object.
# @param i_a node a.
# @return result sin(a).
def forward( io_ctx,
             i_a ):
  io_ctx.save_for_backward( i_a )
  l_result = math.sin(i_a)
  return l_result

## Backward method.
# @param i_ctx context object.
# @param i_grad gradient w.r.t. to output of forward method.
# @return gradient w.r.t. to input of forward method.
def backward( i_ctx,
              i_grad ):
  [l_a] = i_ctx.m_saved_data
  l_grad_a = math.cos(l_a) * i_grad
  return [ l_grad_a ]
