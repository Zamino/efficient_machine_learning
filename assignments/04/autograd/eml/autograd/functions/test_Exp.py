import unittest
from . import Exp
from .. import context

class TestExp( unittest.TestCase ):
  def test_forward( self ):
    l_result = Exp.forward(context.Context(), 0.0)

    self.assertAlmostEqual(l_result, 1.0)

  def test_backward( self ):
    l_context = context.Context()
    l_context.save_for_backward(0.0)
    [l_grad_a]= Exp.backward(l_context, 5.0)

    self.assertEqual( l_grad_a, 5.0 )
