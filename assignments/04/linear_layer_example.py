import torch

W = torch.tensor([[1.0,2.0,3.0],[4.0,5.0,6.0]], requires_grad = True)
X = torch.tensor([[7.0,8.0,9.0],[10.0,11.0,12.0]], requires_grad = True)

Y = torch.matmul(X , W.t())
Y.backward(torch.tensor([[1, 2],[2, 3]]))

print("Y =\n", Y)
print("dLdX =\n", X.grad)
print("dLdW =\n", W.grad)
