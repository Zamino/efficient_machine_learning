import math
import unittest

# f(x,y,z) = x*(y+z)
def f_forward( i_x,
               i_y,
               i_z ):
     l_a = i_y + i_z
     l_b = i_x * l_a

     return l_b

def f_backward( i_x,
                i_y,
                i_z ):
    l_a = i_y + i_z

    l_dbda = i_x
    l_dbdx = l_a

    l_dady = 1
    l_dbdy = l_dbda * l_dady

    l_dadz = 1
    l_dbdz = l_dbda * l_dadz

    return l_dbdx, l_dbdy, l_dbdz

# g(w0,w1,w2,x0,x1) = 1/(1+exp(-1*(w0*x0+w1*x1+w2)))
def g_forward(i_w0,i_w1,i_w2,i_x0,i_x1):
    l_a = i_w0 * i_x0
    l_b = i_w1 * i_x1
    l_c = l_a + l_b
    l_d = l_c + i_w2
    l_e = -l_d
    l_f = math.exp(l_e)
    l_g = 1 + l_f
    l_h = 1 / l_g
    return l_h

def g_backward(i_w0,i_w1,i_w2,i_x0,i_x1):
    l_a = i_w0 * i_x0
    l_b = i_w1 * i_x1
    l_c = l_a + l_b
    l_d = l_c + i_w2
    l_e = -l_d
    l_f = math.exp(l_e)
    l_g = 1 + l_f
    l_h = 1 / l_g

    l_dhdg = -(1/l_g**2)

    l_dgdf = 1
    l_dhdf = l_dhdg * l_dgdf

    l_dfde = math.exp(l_e)
    l_dhde = l_dhdf * l_dfde

    l_dedd = -1
    l_dhdd = l_dhde * l_dedd

    l_dddc = 1
    l_dhdc = l_dhdd * l_dddc

    l_dddw2 = 1
    l_dhdw2 = l_dhdd * l_dddw2

    l_dcda = 1
    l_dhda = l_dhdc * l_dcda

    l_dcdb = 1
    l_dhdb = l_dhdc * l_dcdb

    l_dbdw1 = i_x1
    l_dhdw1 = l_dhdb * l_dbdw1

    l_dbdx1 = i_w1
    l_dhdx1 = l_dhdb * l_dbdx1

    l_dadw0 = i_x0
    l_dhdw0 = l_dhda * l_dadw0

    l_dadx0 = i_w0
    l_dhdx0 = l_dhda * l_dadx0

    return (l_dhdw0, l_dhdw1, l_dhdw2, l_dhdx0, l_dhdx1)

def h_forward(x,y):
    l_a = x*y
    l_b = x+y
    l_c = x-y
    l_d = math.sin(l_a)
    l_e = math.cos(l_b)
    l_f = math.exp(l_c)
    l_g = l_d + l_e
    l_h = l_g / l_f
    return l_h

def h_backward(i_x,i_y):
    l_a = i_x*i_y
    l_b = i_x+i_y
    l_c = i_x-i_y
    l_d = math.sin(l_a) + math.cos(l_b)
    l_e = math.exp(l_c)
    l_h = l_d / l_e


    # l_h = l_d / l_e
    l_dhdd = 1/l_e
    l_dhde = -l_d/(l_e**2)

    # l_d = math.sin(l_a) + math.cos(l_b)
    l_ddda = math.cos(l_a)
    l_dhda = l_dhdd * l_ddda

    # l_d = math.sin(l_a) + math.cos(l_b)
    l_dddb = -math.sin(l_b)
    l_dhdb = l_dhdd * l_dddb

    # l_e = math.exp(l_c)
    l_dedc = math.exp(l_c)
    l_dhdc = l_dhde * l_dedc


    # l_a = i_x*i_y
    # l_b = x+y
    # l_c = x-y
    l_dady = i_x
    l_dbdy = 1
    l_dcdy = -1

    # l_a = i_x*i_y
    # l_b = x+y
    # l_c = x-y
    l_dadx = i_y
    l_dbdx = 1
    l_dcdx = 1

    l_dhdy = l_dhdc * l_dcdy + l_dhdb * l_dbdy + l_dhda * l_dady
    l_dhdx = l_dhdc * l_dcdx + l_dhdb * l_dbdx + l_dhda * l_dadx

    return l_dhdx, l_dhdy

class TestAutograd(unittest.TestCase):
    def test_f(self):
        x = 1; y = 2; z = 3
        self.assertAlmostEqual(x*(y+z), f_forward(x,y,z))
        self.assertAlmostEqual((5, 1, 1), f_backward(x,y,z))

    def test_g(self):
        w0, w1, w2 = 1, 2, 0
        x0, x1 = 2, -1
        self.assertAlmostEqual(1/(1+math.exp(-1*(w0*x0+w1*x1+w2))), g_forward(w0,w1,w2,x0,x1))
        self.assertAlmostEqual((0.5, -0.25, 0.25, 0.25, 0.5), g_backward(w0,w1,w2,x0,x1))
        w0, w1, w2 = 2, 4, 0
        x0, x1 = 4, -2
        self.assertAlmostEqual(1/(1+math.exp(-1*(w0*x0+w1*x1+w2))), g_forward(w0,w1,w2,x0,x1))
        self.assertAlmostEqual((1.0, -0.5, 0.25, 0.5, 1.0), g_backward(w0,w1,w2,x0,x1))

    def test_h(self):
        x, y = 0, 0
        self.assertAlmostEqual((math.sin(x*y) + math.cos(x+y))/math.exp(x-y), h_forward(x,y))
        self.assertAlmostEqual((-1.0, 1.0), h_backward(x,y))
        x, y = 0, 1
        self.assertAlmostEqual((math.sin(x*y) + math.cos(x+y))/math.exp(x-y), h_forward(x,y))
        self.assertAlmostEqual((-1.0377673986356823, -0.8186613472629571), h_backward(x,y))

if __name__ == '__main__':
    unittest.main()
