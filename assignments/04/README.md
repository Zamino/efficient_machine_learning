# Assignment 04

## 5. Autograd

### 5.1. Examples

#### 1

`autograd_examples.py`
```python
import math
import unittest

# f(x,y,z) = x*(y+z)
def f_forward( i_x,
               i_y,
               i_z ):
     l_a = i_y + i_z
     l_b = i_x * l_a

     return l_b

def f_backward( i_x,
                i_y,
                i_z ):
    l_a = i_y + i_z

    l_dbda = i_x
    l_dbdx = l_a

    l_dady = 1
    l_dbdy = l_dbda * l_dady

    l_dadz = 1
    l_dbdz = l_dbda * l_dadz

    return l_dbdx, l_dbdy, l_dbdz

class TestAutograd(unittest.TestCase):
    def test_f(self):
        x = 1; y = 2; z = 3
        self.assertAlmostEqual(x*(y+z), f_forward(x,y,z))
        self.assertAlmostEqual((5, 1, 1), f_backward(x,y,z))
    # ...
```

#### 2

```python
# g(w0,w1,w2,x0,x1) = 1/(1+exp(-1*(w0*x0+w1*x1+w2)))
def g_forward(i_w0,i_w1,i_w2,i_x0,i_x1):
    l_a = i_w0 * i_x0
    l_b = i_w1 * i_x1
    l_c = l_a + l_b
    l_d = l_c + i_w2
    l_e = -l_d
    l_f = math.exp(l_e)
    l_g = 1 + l_f
    l_h = 1 / l_g
    return l_h

def g_backward(i_w0,i_w1,i_w2,i_x0,i_x1):
    l_a = i_w0 * i_x0
    l_b = i_w1 * i_x1
    l_c = l_a + l_b
    l_d = l_c + i_w2
    l_e = -l_d
    l_f = math.exp(l_e)
    l_g = 1 + l_f
    l_h = 1 / l_g

    l_dhdg = -(1/l_g**2)

    l_dgdf = 1
    l_dhdf = l_dhdg * l_dgdf

    l_dfde = math.exp(l_e)
    l_dhde = l_dhdf * l_dfde

    l_dedd = -1
    l_dhdd = l_dhde * l_dedd

    l_dddc = 1
    l_dhdc = l_dhdd * l_dddc

    l_dddw2 = 1
    l_dhdw2 = l_dhdd * l_dddw2

    l_dcda = 1
    l_dhda = l_dhdc * l_dcda

    l_dcdb = 1
    l_dhdb = l_dhdc * l_dcdb

    l_dbdw1 = i_x1
    l_dhdw1 = l_dhdb * l_dbdw1

    l_dbdx1 = i_w1
    l_dhdx1 = l_dhdb * l_dbdx1

    l_dadw0 = i_x0
    l_dhdw0 = l_dhda * l_dadw0

    l_dadx0 = i_w0
    l_dhdx0 = l_dhda * l_dadx0

    return (l_dhdw0, l_dhdw1, l_dhdw2, l_dhdx0, l_dhdx1)

class TestAutograd(unittest.TestCase):
    # ...
    def test_g(self):
        w0, w1, w2 = 1, 2, 0
        x0, x1 = 2, -1
        self.assertAlmostEqual(1/(1+math.exp(-1*(w0*x0+w1*x1+w2))), g_forward(w0,w1,w2,x0,x1))
        self.assertAlmostEqual((0.5, -0.25, 0.25, 0.25, 0.5), g_backward(w0,w1,w2,x0,x1))
        w0, w1, w2 = 2, 4, 0
        x0, x1 = 4, -2
        self.assertAlmostEqual(1/(1+math.exp(-1*(w0*x0+w1*x1+w2))), g_forward(w0,w1,w2,x0,x1))
        self.assertAlmostEqual((1.0, -0.5, 0.25, 0.5, 1.0), g_backward(w0,w1,w2,x0,x1))
    # ...
```

#### 3

```python
def h_forward(x,y):
    l_a = x*y
    l_b = x+y
    l_c = x-y
    l_d = math.sin(l_a)
    l_e = math.cos(l_b)
    l_f = math.exp(l_c)
    l_g = l_d + l_e
    l_h = l_g / l_f
    return l_h

def h_backward(i_x,i_y):
    l_a = i_x*i_y
    l_b = i_x+i_y
    l_c = i_x-i_y
    l_d = math.sin(l_a) + math.cos(l_b)
    l_e = math.exp(l_c)
    l_h = l_d / l_e


    # l_h = l_d / l_e
    l_dhdd = 1/l_e
    l_dhde = -l_d/(l_e**2)

    # l_d = math.sin(l_a) + math.cos(l_b)
    l_ddda = math.cos(l_a)
    l_dhda = l_dhdd * l_ddda

    # l_d = math.sin(l_a) + math.cos(l_b)
    l_dddb = -math.sin(l_b)
    l_dhdb = l_dhdd * l_dddb

    # l_e = math.exp(l_c)
    l_dedc = math.exp(l_c)
    l_dhdc = l_dhde * l_dedc


    # l_a = i_x*i_y
    # l_b = x+y
    # l_c = x-y
    l_dady = i_x
    l_dbdy = 1
    l_dcdy = -1

    # l_a = i_x*i_y
    # l_b = x+y
    # l_c = x-y
    l_dadx = i_y
    l_dbdx = 1
    l_dcdx = 1

    l_dhdy = l_dhdc * l_dcdy + l_dhdb * l_dbdy + l_dhda * l_dady
    l_dhdx = l_dhdc * l_dcdx + l_dhdb * l_dbdx + l_dhda * l_dadx

    return l_dhdx, l_dhdy


class TestAutograd(unittest.TestCase):
    # ...
    def test_h(self):
        x, y = 0, 0
        self.assertAlmostEqual((math.sin(x*y) + math.cos(x+y))/math.exp(x-y), h_forward(x,y))
        self.assertAlmostEqual((-1.0, 1.0), h_backward(x,y))
        x, y = 0, 1
        self.assertAlmostEqual((math.sin(x*y) + math.cos(x+y))/math.exp(x-y), h_forward(x,y))
        self.assertAlmostEqual((-1.0377673986356823, -0.8186613472629571), h_backward(x,y))
```

### 5.2. Engine

See implementation in directory `autograd/`

```
$ python -m unittest
..................
----------------------------------------------------------------------
Ran 18 tests in 0.001s

OK
```

## 6. Custom Extension

### 6.1. From PyTorch to Machine Code

####  1.

![part_1](6_1_1_1.jpeg)
![part_2](6_1_1_2.jpeg)

`./example_linear_layer.py`
```python
import torch

W = torch.tensor([[1.0,2.0,3.0],[4.0,5.0,6.0]], requires_grad = True)
X = torch.tensor([[7.0,8.0,9.0],[10.0,11.0,12.0]], requires_grad = True)

Y = torch.matmul(X , W.t())
Y.backward(torch.tensor([[1, 2],[2, 3]]))

print("Y =\n", Y)
print("dLdX =\n", X.grad)
print("dLdW =\n", W.grad)
```

```
Y =
 tensor([[ 50., 122.],
        [ 68., 167.]], grad_fn=<MmBackward0>)
dLdX =
 tensor([[ 9., 12., 15.],
        [14., 19., 24.]])
dLdW =
 tensor([[27., 30., 33.],
        [44., 49., 54.]])
```

### 6.2 Python Extensions

#### 1, 2, 3

`eml/ext/linear_python.py`
```python
import torch

class Layer(torch.nn.Module):
  def __init__( self,
                i_n_features_input,
                i_n_features_output ):
    super().__init__()
    self.input_features = i_n_features_input
    self.output_features = i_n_features_output

    self.weight = torch.nn.Parameter(
            torch.empty(i_n_features_output, i_n_features_input))

    # Not a very smart way to initialize weights
    torch.nn.init.uniform_(self.weight, -0.1, 0.1)

  def forward(self, i_input):
    o_result = Function.apply(i_input, self.weight)
    return o_result

class Function(torch.autograd.Function):
  @staticmethod
  def forward(io_ctx, i_a, i_b):
    io_ctx.save_for_backward(i_a, i_b)
    o_result = torch.matmul(i_a, i_b.t())
    return o_result

  @staticmethod
  def backward(io_ctx, i_grad_output):
    l_a, l_b = io_ctx.saved_tensors
    l_dlda = torch.matmul(i_grad_output, l_b)
    l_dldb = torch.matmul(i_grad_output.t(), l_a)
    return l_dlda, l_dldb
```

`eml/ext/test_linear_python.py`
```python
import unittest
import torch

from . import linear_python

class TestLinearPython(unittest.TestCase):
  def test_simple(self):
    # test data
    t_W = torch.tensor([[1.0,2.0,3.0],[4.0,5.0,6.0]], requires_grad = True)
    t_X = torch.tensor([[7.0,8.0,9.0],[10.0,11.0,12.0]], requires_grad = True)
    t_Y = torch.matmul(t_X , t_W.t())
    t_Y.backward(torch.tensor([[1, 2],[2, 3]]))

    l_X = torch.tensor([[7.0, 8.0, 9.0],[10.0,11.0,12.0]], requires_grad=True)
    l_layer = linear_python.Layer(3,2)
    # W
    l_layer.weight = torch.nn.Parameter(
            torch.tensor([[1.0,2.0,3.0],[4.0,5.0,6.0]],requires_grad=True))
    l_Y = l_layer(l_X)
    l_Y.backward(torch.tensor([[1.0, 2.0],[2.0, 3.0]]))
    self.assertTrue(torch.equal(t_Y, l_Y))
    self.assertTrue(torch.equal(t_X.grad, l_X.grad))
    self.assertTrue(torch.equal(t_W.grad, l_layer.weight.grad))
```

```
$ python -m unittest
.
----------------------------------------------------------------------
Ran 1 test in 0.266s

OK
```
