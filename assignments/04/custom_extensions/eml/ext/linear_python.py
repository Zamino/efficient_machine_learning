import torch

class Layer(torch.nn.Module):
  def __init__( self,
                i_n_features_input,
                i_n_features_output ):
    super().__init__()
    self.input_features = i_n_features_input
    self.output_features = i_n_features_output

    self.weight = torch.nn.Parameter(
            torch.empty(i_n_features_output, i_n_features_input))

    # Not a very smart way to initialize weights
    torch.nn.init.uniform_(self.weight, -0.1, 0.1)

  def forward(self, i_input):
    o_result = Function.apply(i_input, self.weight)
    return o_result

class Function(torch.autograd.Function):
  @staticmethod
  def forward(io_ctx, i_a, i_b):
    io_ctx.save_for_backward(i_a, i_b)
    o_result = torch.matmul(i_a, i_b.t())
    return o_result

  @staticmethod
  def backward(io_ctx, i_grad_output):
    l_a, l_b = io_ctx.saved_tensors
    l_dlda = torch.matmul(i_grad_output, l_b)
    l_dldb = torch.matmul(i_grad_output.t(), l_a)
    return l_dlda, l_dldb
