import unittest
import torch

from . import linear_python

class TestLinearPython(unittest.TestCase):
  def test_simple(self):
    # test data
    t_W = torch.tensor([[1.0,2.0,3.0],[4.0,5.0,6.0]], requires_grad = True)
    t_X = torch.tensor([[7.0,8.0,9.0],[10.0,11.0,12.0]], requires_grad = True)
    t_Y = torch.matmul(t_X , t_W.t())
    t_Y.backward(torch.tensor([[1, 2],[2, 3]]))

    l_X = torch.tensor([[7.0, 8.0, 9.0],[10.0,11.0,12.0]], requires_grad=True)
    l_layer = linear_python.Layer(3,2)
    # W
    l_layer.weight = torch.nn.Parameter(
            torch.tensor([[1.0,2.0,3.0],[4.0,5.0,6.0]],requires_grad=True))
    l_Y = l_layer(l_X)
    l_Y.backward(torch.tensor([[1.0, 2.0],[2.0, 3.0]]))
    self.assertTrue(torch.equal(t_Y, l_Y))
    self.assertTrue(torch.equal(t_X.grad, l_X.grad))
    self.assertTrue(torch.equal(t_W.grad, l_layer.weight.grad))
