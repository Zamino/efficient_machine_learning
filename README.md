# Efficient Machine Learning

## Assignments

The solutions to the assignments are in the `README.md` files in the
corresponding folder in the `assignments` folder.
